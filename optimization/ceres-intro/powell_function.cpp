#include "ceres/ceres.h"
#include "glog/logging.h"

#include <math.h>
using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;

struct F1 {
   template <typename T>
   bool operator()(const T* const x1, const T* const x2, T* residual) const {
     residual[0] = x1[0] + T(10)*x2[0];
     return true;
   }
};

struct F2 {
   template <typename T>
   bool operator()(const T* const x3, const T* const x4, T* residual) const {
     residual[0] = sqrt(T(5))*(x3[0] - x4[0]);
     return true;
   }
};

struct F3 {
   template <typename T>
   bool operator()(const T* const x2, const T* const x3, T* residual) const {
     residual[0] = pow(x2[0] - T(2)*x3[0], 2);
     return true;
   }
};

struct F4 {
   template <typename T>
   bool operator()(const T* const x1, const T* const x4, T* residual) const {
     residual[0] = sqrt(T(10))*pow(x1[0] - x4[0], 2);
     return true;
   }
};

int main(int argc, char** argv) {
  // google::InitGoogleLogging(argv[0]);

  // The variable to solve for with its initial value.
  double x1 = 4;
  double x2 = 1.54;
  double x3 = 44;
  double x4 = 32.42;

  // Build the problem.
  Problem problem;

  // Set up the only cost function (also known as residual). This uses
  // auto-differentiation to obtain the derivative (jacobian).
  CostFunction* cost_function1 =
      new AutoDiffCostFunction<F1, 1, 1, 1>(new F1);
  CostFunction* cost_function2 =
      new AutoDiffCostFunction<F2, 1, 1, 1>(new F2);
  CostFunction* cost_function3 =
      new AutoDiffCostFunction<F3, 1, 1, 1>(new F3);
  CostFunction* cost_function4 =
      new AutoDiffCostFunction<F4, 1, 1, 1>(new F4);

  problem.AddResidualBlock(cost_function1, NULL, &x1, &x2);
  problem.AddResidualBlock(cost_function2, NULL, &x3, &x4);
  problem.AddResidualBlock(cost_function3, NULL, &x2, &x3);
  problem.AddResidualBlock(cost_function4, NULL, &x1, &x4);

  // Run the solver!
  Solver::Options options;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = true;
  options.max_num_iterations = 100;

 std::cout << "Initial x1 = " << x1
            << ", x2 = " << x2
            << ", x3 = " << x3
            << ", x4 = " << x4
            << "\n";
  // Run the solver!
  Solver::Summary summary;
  Solve(options, &problem, &summary);
  std::cout << summary.FullReport() << "\n";
  std::cout << "Final x1 = " << x1
            << ", x2 = " << x2
            << ", x3 = " << x3
            << ", x4 = " << x4
            << "\n";

  return 0;
}
