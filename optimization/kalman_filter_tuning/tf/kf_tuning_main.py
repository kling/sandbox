import tensorflow as tf

noisy_inputs = []
ground_truth = []


def load_data():
    with open('../position_velocity_noisy_values_100s.csv') as in_file:
        for line in in_file:
            noisy_inputs.append(line.split(','))

    with open('../position_velocity_true_values_100s.csv') as in_file:
        for line in in_file:
            ground_truth.append(line.split(','))



def build_kf_network(input):
    with tf.name_scope('typecast_input'):
        input = tf.cast(input, tf.float64)



def run_kf_training():
    load_data()

    kf_inputs = tf.placeholder(tf.float32, [])


    a = tf.constant([[1., 2., 3.], [1.,2.,3.]])
    b = tf.constant([[3.],[1.], [1.]])

    ab = tf.matmul(a, b)

    with tf.Session() as sess:
        result = sess.run(ab)
        print(result)




if __name__ == '__main__':
    run_kf_training()