import unittest
import kalman.KalmanFilterBase as kfBase
import numpy as np

class KalmanFilterBaseTests(unittest.TestCase):
    def test_initial_state_and_covariance_mismatch(self):
        state = np.matrix([1,2,3]).getT()
        covariance = np.matrix([[1,2], [1,2]])

        try:
            kfBase.KalmanFilterBase(state, covariance)
            self.fail('Expected constructor AssertionError did not happen.')
        except AssertionError:
            pass

    def test_state_vector_dimension_asserts(self):
        state = np.matrix([1,2]).getT()
        covariance = np.matrix([[1,2,3], [1,2,3]])

        try:
            kfBase.KalmanFilterBase(state, covariance)
            self.fail('Expected constructor AssertionError did not happen.')
        except AssertionError:
            pass
