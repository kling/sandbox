import tensorflow as tf

#class KalmanFilterBase:
    # def __init__(self, initial_mean, initial_covariance):
        # assert type(initial_mean) is np.matrix
        # assert type(initial_covariance) is np.matrix
        #
        # assert initial_mean.shape[0] == initial_covariance.shape[0], 'State and covariance rank mismatch.'
        # assert initial_mean.shape[1] == 1, 'State vector dimensions are incorrect.'
        # assert initial_covariance.shape[0] == initial_covariance.shape[1], 'Covariance matrix is not square.'

def predict_state(mean, A_matrix):

def prediction_covariance_update(sigma, A_matrix, Q_matrix):
    return tf.matmul(tf.matmul(A_matrix, sigma), tf.transpose(A_matrix)) + Q_matrix


def measurement_covariance_update(sigma, C_matrix, R_matrix):
    temp = tf.matmul(tf.matmul(C_matrix, sigma), tf.transpose(C_matrix)) + R_matrix
    K_matrix = tf.matmul(tf.matmul(sigma, tf.transpose(C_matrix)), tf.matrix_inverse(temp))
    KC_matrix = tf.matmul(K_matrix, C_matrix)
    eye_minus_KC = tf.eye(tf.shape(KC_matrix)[0]) - KC_matrix
    return tf.matmul(eye_minus_KC, sigma)


    #self.mean += K_matrix*innovation_vector
