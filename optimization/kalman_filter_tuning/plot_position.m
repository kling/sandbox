ground_truth = csvread('position_velocity_true_values.csv');
kf_output = csvread('kalman_filter_tuning/kf_output.csv');

figure();
grid on;
hold on;
plot(ground_truth(:,2), ground_truth(:,3), '-db')
plot(kf_output(:,2), kf_output(:,3), '--xr')

rmse_x = sqrt(mean((ground_truth(:,2) - kf_output(:,2)).^2))
rmse_y = sqrt(mean((ground_truth(:,3) - kf_output(:,3)).^2))