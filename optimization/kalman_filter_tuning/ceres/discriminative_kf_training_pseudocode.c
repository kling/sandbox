#include "ceres/ceres.h"
#include "glog/logging.h"
using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;


// Residual of the kalman filter at time t
struct KF_t_residual {
  KF_t_residual(inputs x, measurements y, ground_truth true_y, int t){}
  template <typename T> bool operator()(const T* const R_matrix,
                                        const T* const Q_matrix,
                                        T* residual) const {

    mu = initial_mu;
    for (int i = 0; i < t; i++) {
      mu = kf_prediction(mu, x(t), R_matrix);
      mu = kf_measurement_update(mu, y(t), Q_matrix);
    }
    residual = ground_truth - mu;
    return true;
  }
 private:
  ...
};


int main(int argc, char** argv) {
  Martix R_matrix, Q_matrix;

  Problem problem;

  CostFunction* cost_function =
      new AutoDiffCostFunction<CostFunctor, 1, 1>(new CostFunctor);

  for (int i = 0; i < t; i++) {
    KF_t_residual residual = new KF_t_residual(input_list, measurements_list, ground_truth_list, i);
    problem.AddResidualBlock(new AutoDiffCostFunction<KF_t_residual, ...>(residual), NULL, &R_matrix, &Q_matrix);
  }
  

  // Run the solver!
  Solver::Options options;
  options.linear_solver_type = ceres::SPARSE_QR;
  options.minimizer_progress_to_stdout = true;
  Solver::Summary summary;
  Solve(options, &problem, &summary);

  return 0;
}
