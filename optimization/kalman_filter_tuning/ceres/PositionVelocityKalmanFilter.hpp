#pragma once

#include "KalmanFilterBase.hpp"
#include "PositionVelocityCsvParser.hpp"

template <typename T>
class PositionVelocityKalmanFilter : public KalmanFilterBase <T, 4, 4>
{
 public:
  PositionVelocityKalmanFilter(const Eigen::Matrix<T, 4, 1>& initialState);

  void PredictionUpdate(const Eigen::Matrix<T, 4, 4>& rMatrix);

  void MeasurementUpdate(const Eigen::Matrix<T, 4, 1>& measurement, const Eigen::Matrix<T, 4, 4>& qMatrix);
};


template <typename T>
PositionVelocityKalmanFilter<T>::PositionVelocityKalmanFilter(const Eigen::Matrix<T, 4, 1>& initialState) {
  this->SetStateEstimates(initialState);

  this->SetStateCovariances(Eigen::Matrix<T, 4, 4>::Identity());
}

template <typename T>
void PositionVelocityKalmanFilter<T>::PredictionUpdate(const Eigen::Matrix<T, 4, 4>& R_matrix) {
  Eigen::Matrix<T, 4, 4> A_matrix;
  A_matrix << T(1), T(0), T(0.1), T(0),
              T(0), T(1), T(0), T(0.1),
              T(0), T(0), T(1), T(0),
              T(0), T(0), T(0), T(1);

  Eigen::Matrix<T, 4, 1> predictedState = (A_matrix * this->GetMeanStates());

  this->PredictionCovarianceUpdate(A_matrix, predictedState, R_matrix);
}

template <typename T>
void PositionVelocityKalmanFilter<T>::MeasurementUpdate(const Eigen::Matrix<T, 4, 1>& measurement,
    const Eigen::Matrix<T, 4, 4>& Q_matrix) {
  Eigen::Matrix<T, 4, 4> C_matrix = Eigen::Matrix<T, 4, 4>::Identity();
  Eigen::Matrix<T, 4, 1> predictedMeasurement = (C_matrix * this->GetMeanStates());
  this->MeasurementCovarianceUpdate(C_matrix, measurement, predictedMeasurement, Q_matrix);
}
