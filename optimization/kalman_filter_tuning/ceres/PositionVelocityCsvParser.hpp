#ifndef OPTIMIZATION_KALMAN_FILTER_TUNING_POSITION_VELOCITY_CSV_PARSER_HPP_
#define OPTIMIZATION_KALMAN_FILTER_TUNING_POSITION_VELOCITY_CSV_PARSER_HPP_

#include <string>
#include <fstream>

typedef struct State2D {
	float time;
	float positionX;
	float positionY;
	float velocityX;
	float velocityY;
} State2D;

class PositionVelocityCsvParser {
  public:
  	PositionVelocityCsvParser(const std::string& filename);
  	bool ReadNextValue(State2D& next_state);

  private:
  	std::ifstream inputFile;
};

#endif // OPTIMIZATION_KALMAN_FILTER_TUNING_POSITION_VELOCITY_CSV_PARSER_HPP_
