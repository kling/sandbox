#ifndef KALMAN_FILTER_BASE_HPP_
#define KALMAN_FILTER_BASE_HPP_

#include <Eigen/Core>
#include <Eigen/LU>


/**
 * Implementation of Kalman Filter equations and encapsulation of state
 * estimates and covariances.
 */
template <typename T, int NUM_STATES, int NUM_MEASUREMENTS>
class KalmanFilterBase
{
 public:
  /**
   * Constructor for the abstract KalmanFilterBase class. Mean state estimate
   * and covariance matrices will be initialized to dimensions according to
   * the number of states specified.
   **/
  KalmanFilterBase();

  /**
   * Accessor to get the mean state estimates of the Kalman filter.
   */
  Eigen::Matrix<T, NUM_STATES, 1> GetMeanStates();

  /**
   * Accessor to get the state estimate covariances of the Kalman filter.
   */
  Eigen::Matrix<T, NUM_STATES, NUM_STATES> GetStateCovariances();

 protected:
  /**
   * Performs the Kalman filter prediction update by updating the mean
   * estimates to predicted_state and calculating the covariances for the
   * new estimates. It is up to the calling method to provide the state
   * prediction based on the mean state estimates.
   *
   * @param[in] matr_A The state space 'A' matrix, or the linearized state
   *    transition matrix for the nonlinear case.
   * @param[in] predicted_state The predicted states, calculated based on the
   *    previous mean state estimates.
   * @param[in] process_noise The 'R' covariance matrix for the system's
   *    process noise.
   **/
  void PredictionCovarianceUpdate(const Eigen::Matrix<T, NUM_STATES, NUM_STATES>& matr_A, 
      const Eigen::Matrix<T, NUM_STATES, 1>& predicted_state,
      const Eigen::Matrix<T, NUM_STATES, NUM_STATES>& process_noise);

  /**
   * Performs the Kalman filter measurement update. The calling method must
   * calculate the predicted measurement based on the current state estimate
   * first.
   *
   * @param[in] matr_C The state space 'C' matrix, or the linearized
   *    measurement matrix for the nonlinear case.
   * @param[in] measurements Vector of new measurements for the system.
   * @param[in] pred_measurements Vector of predicted measurements.
   * @param[in] measurement_noise The 'Q' covariance matrix for the system's
   *    measurement noise.
   */
  void MeasurementCovarianceUpdate(const Eigen::Matrix<T, NUM_MEASUREMENTS, NUM_STATES>& matr_C,
      const Eigen::Matrix<T, NUM_MEASUREMENTS, 1>& measurements,
      const Eigen::Matrix<T, NUM_MEASUREMENTS, 1>& pred_measurements,
      const Eigen::Matrix<T, NUM_MEASUREMENTS, NUM_MEASUREMENTS> measurement_noise);

  /**
   * Set Kalman filter state estimates. This method should be used to
   * reinitialize the state estimate.
   *
   * @param new_state New mean estimates for the Kalman filter.
   */
  void SetStateEstimates(const Eigen::Matrix<T, NUM_STATES, 1>& new_state);

  /**
   * Set Kalman filter state covariances. This method should be used to
   * reinitialize the state covariances.
   *
   * @param new_covariances New state estimate covariances for the Kalman
   *    filter.
   */
  void SetStateCovariances(const Eigen::Matrix<T, NUM_STATES, NUM_STATES>& new_covariances);

private:

  /**
   * The mean estimate of the states.
   */
  Eigen::Matrix<T, NUM_MEASUREMENTS, 1> mean_;

  /**
   * Covariances for the current mean estimates.
   */
  Eigen::Matrix<T, NUM_STATES, NUM_STATES> sigma_;
};


template <typename T, int NUM_STATES, int NUM_MEASUREMENTS>
KalmanFilterBase<T, NUM_STATES, NUM_MEASUREMENTS>::KalmanFilterBase() {
  mean_ = Eigen::Matrix<T, NUM_STATES, 1>::Zero();
  sigma_ = Eigen::Matrix<T, NUM_STATES, NUM_STATES>::Zero();
}


template <typename T, int NUM_STATES, int NUM_MEASUREMENTS>
Eigen::Matrix<T, NUM_STATES, 1> KalmanFilterBase<T, NUM_STATES, NUM_MEASUREMENTS>::GetMeanStates() {
  return mean_;
}


template <typename T, int NUM_STATES, int NUM_MEASUREMENTS>
Eigen::Matrix<T, NUM_STATES, NUM_STATES> KalmanFilterBase<T, NUM_STATES, NUM_MEASUREMENTS>::GetStateCovariances() {
  return sigma_;
}


template <typename T, int NUM_STATES, int NUM_MEASUREMENTS>
void KalmanFilterBase<T, NUM_STATES, NUM_MEASUREMENTS>::PredictionCovarianceUpdate(
    const Eigen::Matrix<T, NUM_STATES, NUM_STATES>& matr_A, 
    const Eigen::Matrix<T, NUM_STATES, 1>& predicted_state,
    const Eigen::Matrix<T, NUM_STATES, NUM_STATES>& process_noise)
{
  mean_ = predicted_state;
  sigma_ = matr_A*sigma_*matr_A.transpose() + process_noise;
}


template <typename T, int NUM_STATES, int NUM_MEASUREMENTS>
void KalmanFilterBase<T, NUM_STATES, NUM_MEASUREMENTS>::MeasurementCovarianceUpdate(
    const Eigen::Matrix<T, NUM_MEASUREMENTS, NUM_STATES>& matr_C,
    const Eigen::Matrix<T, NUM_MEASUREMENTS, 1>& measurements,
    const Eigen::Matrix<T, NUM_MEASUREMENTS, 1>& pred_measurements,
    const Eigen::Matrix<T, NUM_MEASUREMENTS, NUM_MEASUREMENTS> measurement_noise)
{
  Eigen::Matrix<T, NUM_MEASUREMENTS, NUM_MEASUREMENTS> temp;
  temp = matr_C * sigma_ * matr_C.transpose() + measurement_noise;

  // Possible error here in matrix sizes
  Eigen::Matrix<T, NUM_MEASUREMENTS, NUM_MEASUREMENTS> matr_K;
  matr_K = (sigma_ * matr_C.transpose() * temp.inverse());

  mean_ = mean_ + matr_K*(measurements - pred_measurements);
  sigma_ = (Eigen::Matrix<T, NUM_STATES, NUM_STATES>::Identity()
      - matr_K*matr_C) * sigma_;
}


template <typename T, int NUM_STATES, int NUM_MEASUREMENTS>
void KalmanFilterBase<T, NUM_STATES, NUM_MEASUREMENTS>::SetStateEstimates(const Eigen::Matrix<T, NUM_STATES, 1>& new_state)
{
  mean_ = new_state;
}


template <typename T, int NUM_STATES, int NUM_MEASUREMENTS>
void KalmanFilterBase<T, NUM_STATES, NUM_MEASUREMENTS>::SetStateCovariances(
    const Eigen::Matrix<T, NUM_STATES, NUM_STATES>& new_covariances)
{
  sigma_ = new_covariances;
}

#endif // KALMAN_FILTER_BASE_HPP_
