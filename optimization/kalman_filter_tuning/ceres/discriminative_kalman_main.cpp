#include "ceres/ceres.h"
#include "glog/logging.h"

#include <Eigen/Dense>

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include "PositionVelocityCsvParser.hpp"
#include "KalmanFilterBase.hpp"
#include "PositionVelocityKalmanFilter.hpp"

using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;

std::vector<State2D> measurements;
std::vector<State2D> groundTruth;


struct KFResidual {
  KFResidual(std::vector<State2D>& m, std::vector<State2D>& gt, int iteration):
      measurements_(m), groundTruth_(gt), iteration_(iteration) {}

  template <typename T>
  bool operator()(const T* const Q_vector, const T* const R_vector, T* residual) const {
    Eigen::Matrix<T, 4, 1> initialState;
    initialState << T(groundTruth[0].positionX), T(groundTruth[0].positionY),
        T(groundTruth[0].velocityX), T(groundTruth[0].velocityY);
    PositionVelocityKalmanFilter<T> kf(initialState);

    Eigen::Matrix<T, 4, 4> Q_matrix;
    Q_matrix << T(Q_vector[0]), T(0), T(0), T(0),
                T(0), T(Q_vector[1]), T(0), T(0),
                T(0), T(0), T(Q_vector[2]), T(0),
                T(0), T(0), T(0), T(Q_vector[3]);
    Eigen::Matrix<T, 4, 4> R_matrix;
    R_matrix << T(R_vector[0]), T(0), T(0), T(0),
                T(0), T(R_vector[1]), T(0), T(0),
                T(0), T(0), T(R_vector[2]), T(0),
                T(0), T(0), T(0), T(R_vector[3]);

    for (int i = 0; i < iteration_; i++) {
      kf.PredictionUpdate(Q_matrix);

      Eigen::Matrix<T, 4, 1> measurementVector;
      measurementVector << T(measurements[i].positionX), T(measurements[i].positionY),
          T(measurements[i].velocityX), T(measurements[i].velocityY);
      kf.MeasurementUpdate(measurementVector, R_matrix);
    }

    auto mean = kf.GetMeanStates();
    residual[0] = T(groundTruth[iteration_].positionX) - mean(0, 0);
    residual[1] = T(groundTruth[iteration_].positionY) - mean(1, 0);
    residual[2] = T(groundTruth[iteration_].velocityX) - mean(2, 0);
    residual[3] = T(groundTruth[iteration_].velocityY) - mean(3, 0);
    return true;
  }

 private:
  std::vector<State2D>& measurements_;
  std::vector<State2D>& groundTruth_;
  int iteration_;
};


void ReadAllData() {
  PositionVelocityCsvParser measurementsParser("../position_velocity_noisy_values.csv");
  PositionVelocityCsvParser groundTruthParser("../position_velocity_true_values.csv");

  measurements.clear();
  groundTruth.clear();

  State2D state;
  bool readSuccess = measurementsParser.ReadNextValue(state);
  while (readSuccess) {
    measurements.push_back(state);
    readSuccess = measurementsParser.ReadNextValue(state);
  }

  readSuccess = groundTruthParser.ReadNextValue(state);
  while (readSuccess) {
    groundTruth.push_back(state);
    readSuccess = groundTruthParser.ReadNextValue(state);
  }
}

void RunTraining() {
  std::cout << "Running Training" << std::endl;
  // Initial values.
  double Q_vector[4] = {0.1, 0.1, 0.1, 0.1};
  double R_vector[4] = {0.1, 0.1, 0.1, 0.1};
  Problem problem;
  
  std::cout << "Building problem..." << std::endl;
  for (int i = 0; i < measurements.size(); i++) {
    CostFunction* cost_function =
        new AutoDiffCostFunction<KFResidual, 4, 4, 4>(
          new KFResidual(measurements, groundTruth, i));

    problem.AddResidualBlock(cost_function, NULL,
        reinterpret_cast<double*>(Q_vector),
        reinterpret_cast<double*>(R_vector));

    if (!(i % 500)) {
      std::cout << "On time step " << i << std::endl;  
    }
  }

  std::cout << "Running solver." << std::endl;
  Solver::Options options;
  options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = true;
  Solver::Summary summary;
  Solve(options, &problem, &summary);

  std::cout << summary.BriefReport() << "\n";;
  std::cout << "Final  Q: " << Q_vector[0] << ", " << Q_vector[1] 
      << ", " << Q_vector[2] << ", " << Q_vector[3] << "\n";
  std::cout << "Final  R: " << R_vector[0] << ", " << R_vector[1] 
      << ", " << R_vector[2] << ", " << R_vector[3] << "\n";
}

void RunAndLogKalmanFilter() {
  std::cout << "Running Kalman Filter" << std::endl;
  Eigen::Matrix<double, 4, 4> Q_matrix;
  Q_matrix << 0.1, 0, 0, 0,
              0, 0.1, 0, 0,
              0, 0, 0.1, 0,
              0, 0, 0, 0.1;
  Eigen::Matrix<double, 4, 4> R_matrix;
  R_matrix << 0.1, 0, 0, 0,
              0, 0.1, 0, 0,
              0, 0, 0.1, 0,
              0, 0, 0, 0.1;

  std::ofstream outfile;
  outfile.open("kf_output.csv");

  Eigen::Matrix<double, 4, 1> initialState;
  initialState << groundTruth[0].positionX, groundTruth[0].positionY,
      groundTruth[0].velocityX, groundTruth[0].velocityY;
  PositionVelocityKalmanFilter<double> kf(initialState);

  for (int i = 0; i < measurements.size(); i++) {
    kf.PredictionUpdate(Q_matrix);

    Eigen::Matrix<double, 4, 1> measurementVector;
    measurementVector << measurements[i].positionX, measurements[i].positionY,
        measurements[i].velocityX, measurements[i].velocityY;
    kf.MeasurementUpdate(measurementVector, R_matrix);

    auto mean = kf.GetMeanStates();

    outfile << measurements[i].time << "," <<
        mean(0, 0) << "," <<
        mean(1, 0) << "," <<
        mean(2, 0) << "," <<
        mean(3, 0) << std::endl;
  }

  outfile.close();
  std::cout << "Done running Kalman Filter." << std::endl;
}

int main(int argc, char** argv) {
  ReadAllData();

  RunAndLogKalmanFilter();
  RunTraining();

  return 0;
}
