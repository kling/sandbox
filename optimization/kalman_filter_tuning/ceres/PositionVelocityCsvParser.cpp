#include "PositionVelocityCsvParser.hpp"
#include <iostream>
#include <stdlib.h>



PositionVelocityCsvParser::PositionVelocityCsvParser(const std::string& filename):inputFile(filename.c_str()) {
	// Empty
}
  	

bool PositionVelocityCsvParser::ReadNextValue(State2D& next_state) {
	std::string s;
	static const std::string delimeter(",");
	if (!getline(inputFile, s)) {
		
		return false;
	}

	size_t index = 0;
	index = s.find(delimeter);
	next_state.time = atof(s.substr(0, index).c_str());
	s.erase(0, index + delimeter.length());

	index = s.find(delimeter);
	next_state.positionX = atof(s.substr(0, index).c_str());
	s.erase(0, index + delimeter.length());

	index = s.find(delimeter);
	next_state.positionY = atof(s.substr(0, index).c_str());
	s.erase(0, index + delimeter.length());

	index = s.find(delimeter);
	next_state.velocityX = atof(s.substr(0, index).c_str());
	s.erase(0, index + delimeter.length());

	index = s.find(delimeter);
	next_state.velocityY = atof(s.substr(0, index).c_str());

	return true;
}