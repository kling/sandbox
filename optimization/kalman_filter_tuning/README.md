This project demonstrates two attempts at implementing Discriminative Training of Kalman Filters, based on the [paper by Abbeel et al.](http://www.andrewng.org/portfolio/discriminative-training-of-kalman-filters/).

The first attempt was done with Ceres. Unfortunately that approach takes a very long time to converge as Ceres as not well sorted to this type of optimization. The problem is that at each step the entire Kalman filter is rerun and added as the residual for the Ceres solver. The complexity of the problem here is essentially N! for N timesteps, so it quickly gets out of hand.

The second approach is using Tensorflow and treating it as a backpropagation through time problem.
