% Create true position and velocity data
time = 0:0.1:500;
position_x = 100 * sin(0.02 .* time);
position_y = 50*sin(0.03.*time) + 200*cos(0.01.*time);

velocity_x = 0.02*100*cos(0.02 .* time);
velocity_y = 0.03*50*cos(0.03.*time) - 0.01*200*sin(0.01.*time);

integrated_velocity_x = cumsum(velocity_x)*0.1 + position_x(1);
integrated_velocity_y = cumsum(velocity_y)*0.1 + position_y(1);

% Noisy signals
noisy_position_x = normrnd(position_x, 0.7);
noisy_position_y = normrnd(position_y, 0.7);
noisy_velocity_x = normrnd(velocity_x, 0.2);
noisy_velocity_y = normrnd(velocity_y, 0.2);


% Plots for sanity checks
figure(1)
hold on
plot(position_x, position_y)
plot(integrated_velocity_x, integrated_velocity_y, 'r')

figure(2)
hold on;
plot(velocity_x, velocity_y)

figure(3)
hold on;
plot(time, noisy_position_x);
plot(time, position_x, 'r');

pos_x_noise_only = noisy_position_x - position_x;

figure(4)
hist(pos_x_noise_only)

true_values = [time', position_x', position_y', velocity_x', velocity_y'];
noisy_values = [time', noisy_position_x', noisy_position_y', noisy_velocity_x', noisy_velocity_y'];

csvwrite('position_velocity_true_values.csv', true_values);
csvwrite('position_velocity_noisy_values.csv', noisy_values);