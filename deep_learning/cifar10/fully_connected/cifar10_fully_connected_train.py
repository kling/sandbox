import tensorflow as tf
import deep_fully_connected as dfc
import cifar_helpers as ch

NUM_EPOCHS = 50
BATCH_SIZE = 50 # The entire test set

SAVE_FILE = './save/fc_model_params.ckpt'

TRAINING_RECORDS = ['../cifar_training_set_1.tfrecords',
                    '../cifar_training_set_2.tfrecords',
                    '../cifar_training_set_3.tfrecords',
                    '../cifar_training_set_4.tfrecords',
                    '../cifar_training_set_5.tfrecords']


def main():
    print('CIFAR10 Fully Connected Training')

    image_batch, label_batch = ch.cifar_input_pipeline(TRAINING_RECORDS, num_epochs=NUM_EPOCHS, batch_size=BATCH_SIZE)

    print('Construct feedforward graph...')
    nn_output, is_training = dfc.fully_connected_feedforward_with_batch_normalization(image_batch)

    train_step, accuracy = ch.cost_function_and_accuracy(nn_output, label_batch)

    ch.run_training(train_step, accuracy, is_training, SAVE_FILE)


if __name__ == '__main__':
    main()