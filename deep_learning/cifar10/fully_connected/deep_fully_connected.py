import tensorflow as tf
import numpy as np
from PIL import Image
from tensorflow.contrib.layers import batch_norm
from tensorflow.contrib.layers import fully_connected
from datetime import datetime

NUM_NODES_HIDDEN_LAYER = 5000

def weight_variable(shape):
  """weight_variable generates a weight variable of a given shape."""
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial, name='W')


def bias_variable(shape):
  """bias_variable generates a bias variable of a given shape."""
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial, name='b')

def fully_connected_layer(input, input_dimension, nodes):
    with tf.name_scope('fully_connected'):
        weight = weight_variable([input_dimension, nodes])
        bias = bias_variable([nodes])
        return tf.matmul(input, weight) + bias

def fully_connected_feedforward_with_batch_normalization(image):
    is_training = tf.placeholder(tf.bool, shape=(), name='is_training')
    bn_params = {
        'is_training': is_training,
        'decay': 0.99,
        'updates_collections': None
    }
    tf.summary.image('input', image)

    with tf.name_scope('reshape_input'):
        input = tf.reshape(image, [-1, 32 * 32 * 3])
        input = tf.cast(input, tf.float32)

    hidden1 = fully_connected(input, NUM_NODES_HIDDEN_LAYER, scope="fc1", normalizer_fn=batch_norm,
                              normalizer_params=bn_params)
    hidden2 = fully_connected(hidden1, NUM_NODES_HIDDEN_LAYER, scope="fc2", normalizer_fn=batch_norm,
                              normalizer_params=bn_params)
    output = fully_connected(hidden2, 10, scope="output", activation_fn=None, normalizer_fn=batch_norm,
                              normalizer_params=bn_params)

    return output, is_training
