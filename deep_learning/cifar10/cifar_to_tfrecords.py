import pickle
from PIL import Image
import numpy as np
from tqdm import tqdm

import multiprocessing as mp

import tensorflow as tf


CIFAR_DIR = 'cifar-10-batches-py/'
CIFAR_META_FILE = CIFAR_DIR + 'batches.meta'
CIFAR_DATA_FILE_PREFIX = CIFAR_DIR + 'data_batch_'
CIFAR_NUM_DATA_FILE = 5
CIFAR_TEST_FILE = CIFAR_DIR + 'test_batch'

# Returns a list with all the CIFAR data filenames
def cifar_data_file_list():
    filenames = []
    for i in range(1, CIFAR_NUM_DATA_FILE+1):
        filenames.append(CIFAR_DATA_FILE_PREFIX + str(i))
    return  filenames

def cifar_tfrecord_file_list():
    filenames = []
    for i in range(1, CIFAR_NUM_DATA_FILE+1):
        filenames.append('cifar_training_set_' + str(i) + '.tfrecords')
    return  filenames

def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict

# Creates a PIL Image object from the input array. Assumes CIFAR images.
def create_image_from_array(pixel_array):
    rgb_arrays = np.reshape(pixel_array, (32, 32, 3), order='F')
    return Image.fromarray(rgb_arrays, 'RGB')

def convert_cifar_to_tf_records(cifar_dict, output_tfrecord_filename):
    tfrecord_writer = tf.python_io.TFRecordWriter(output_tfrecord_filename)
    sample_indexes = list(range(len(cifar_dict['labels'.encode('ascii')])))
    np.random.shuffle(sample_indexes)

    for idx in tqdm(sample_indexes):
        unprocessed_image = cifar_dict['data'.encode('ascii')][idx]
        reshaped_image = np.reshape(unprocessed_image, (32, 32, 3), order='F')
        features = reshaped_image.tostring()

        label = cifar_dict['labels'.encode('ascii')][idx]

        # construct the Example proto boject
        example = tf.train.Example(
            # Example contains a Features proto object
            features=tf.train.Features(
                # Features contains a map of string to Feature proto objects
                feature={
                    # A Feature contains one of either a int64_list,
                    # float_list, or bytes_list
                    'label': tf.train.Feature(
                        int64_list=tf.train.Int64List(value=[label])),
                    'image': tf.train.Feature(
                        bytes_list=tf.train.BytesList(value=[features])),
                }))
        serialized = example.SerializeToString()
        tfrecord_writer.write(serialized)

    return


def main():
    print('Reading in CIFAR data...')
    data_files = cifar_data_file_list()
    tfrecord_output_files = cifar_tfrecord_file_list()

    cifar_train_dictionaries = []
    for file in data_files:
        cifar_train_dictionaries.append(unpickle(file))

    cifar_test_data = unpickle(CIFAR_TEST_FILE)

    pool = mp.Pool()

    print('Setting up threads...')
    starmap_args = []
    for i in range(CIFAR_NUM_DATA_FILE):
        starmap_args.append([cifar_train_dictionaries[i], tfrecord_output_files[i]])

    starmap_args.append([cifar_test_data, 'cifar_test.tfrecords'])

    print('Running TFRecord creation threads...')
    pool.starmap(convert_cifar_to_tf_records, starmap_args)

    print('Done.')


if __name__ == '__main__':
    main()