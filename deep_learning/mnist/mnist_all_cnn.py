import tensorflow as tf


def deepnn(x):
  x_image = tf.reshape(x, [-1, 28, 28, 1])
  x = x_image

  is_training = tf.placeholder(tf.bool, shape=(), name='is_training')

  activation = tf.nn.relu

  x = tf.layers.dropout(x, rate=0.2, training=is_training)
  x = tf.layers.conv2d(x, 96, [3, 3], activation=activation, padding='same')
  x = tf.layers.conv2d(x, 96, [3, 3], activation=activation, padding='same')
  x = tf.layers.conv2d(x, 96, [3, 3], activation=activation, padding='same', strides=(2,2))

  x = tf.layers.dropout(x, rate=0.5, training=is_training)
  x = tf.layers.conv2d(x, 192, [3, 3], activation=activation, padding='same')
  x = tf.layers.conv2d(x, 192, [3, 3], activation=activation, padding='same')
  x = tf.layers.conv2d(x, 192, [3, 3], activation=activation, padding='same', strides=(2,2))

  x = tf.layers.dropout(x, rate=0.5, training=is_training)
  x = tf.layers.conv2d(x, 192, [3, 3], activation=activation, padding='valid')
  x = tf.layers.conv2d(x, 192, [1, 1], activation=activation, padding='valid')
  x = tf.layers.conv2d(x, 10, [1, 1], activation=activation, padding='valid')

  x = tf.layers.average_pooling2d(x, [5,5], 1, padding='valid')
  output = tf.reshape(x, [-1, 10])
  return output, is_training
