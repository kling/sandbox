import argparse
import sys

from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf

import mnist_helpers
import mnist_all_cnn as mnist_all_cnn

def deepnn(x):
  is_training = tf.placeholder(tf.bool, name='is_training')
  bn_params = {
    'is_training': is_training,
    'decay': 0.99,
    'updates_collections': None
  }

  x_image = tf.reshape(x, [-1, 28, 28, 1])

  h_conv1 = tf.layers.conv2d(x_image, 32, [5,5], activation=None, padding='same')
  bn1 = tf.layers.batch_normalization(h_conv1, training=is_training)
  h_bn1 = tf.nn.relu(bn1)
  h_pool1 = tf.layers.max_pooling2d(h_bn1, [2, 2], 2);

  h_conv2 = tf.layers.conv2d(h_pool1, 64, [5,5], activation=None, padding='same')
  bn2 = tf.layers.batch_normalization(h_conv2, training=is_training)
  h_bn2 = tf.nn.relu(bn2)
  h_pool2 = tf.layers.max_pooling2d(h_bn2, [2, 2], 2);

  h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])

  # Fully connected layer 1 -- after 2 round of downsampling, our 28x28 image
  # is down to 7x7x64 feature maps -- maps this to 1024 features.
  h_fc1 = tf.layers.dense(h_pool2_flat, 1024, activation=tf.nn.relu)

  # Dropout - controls the complexity of the model, prevents co-adaptation of
  # features.
  h_fc1_drop = tf.layers.dropout(h_fc1, rate=0.5, training=is_training)

  # Map the 1024 features to 10 classes, one for each digit
  output = tf.layers.dense(h_fc1_drop, 10, activation=None)

  return output, is_training




def main(_):
  # Import data
  mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)

  # Create the model
  x = tf.placeholder(tf.float32, [None, 784])

  # Define loss and optimizer
  y_ = tf.placeholder(tf.float32, [None, 10])

  # Build the graph for the deep net
  nn_one_hot_outputs, is_training = mnist_all_cnn.deepnn(x)

  train_step, correct_prediction, batch_accuracy = mnist_helpers.add_accuracy_metrics(nn_one_hot_outputs, y_)


  with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for i in range(2000):
      batch = mnist.train.next_batch(30)
      if i % 100 == 0:
        train_accuracy = batch_accuracy.eval(feed_dict={
            x: batch[0], y_: batch[1], is_training: False})
        print('step %d, training accuracy %g' % (i, train_accuracy))
      train_step.run(feed_dict={x: batch[0], y_: batch[1],  is_training: True})

    BATCH_SIZE = 2000;
    num_correct = 0
    for i in range(5):
      current_indexes = list(range(i*BATCH_SIZE,(i+1)*BATCH_SIZE))
      predictions = correct_prediction.eval(
        feed_dict={x: mnist.test.images[current_indexes], y_: mnist.test.labels[current_indexes], is_training: False})
      num_correct += sum(predictions)

    print('test accuracy {}'.format(num_correct / len(mnist.test.images)))

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--data_dir', type=str,
                      default='/tmp/tensorflow/mnist/input_data',
                      help='Directory for storing input data')
  FLAGS, unparsed = parser.parse_known_args()
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)