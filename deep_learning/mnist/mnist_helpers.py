import tensorflow as tf

def add_accuracy_metrics(nn_one_hot_output, labels):
    cross_entropy = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=nn_one_hot_output))
    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
    correct_prediction = tf.equal(tf.argmax(nn_one_hot_output, 1), tf.argmax(labels, 1))
    batch_accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    return train_step, correct_prediction, batch_accuracy
