import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('../toronto_downtown.jpg')
img2 = cv2.imread('../toronto_downtown_rotated_and_scaled.jpg')

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
gray2 = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)

sift = cv2.xfeatures2d.SIFT_create()
kp, des = sift.detectAndCompute(gray, None)
kp2, des2 = sift.detectAndCompute(gray2, None)

# cv2.drawKeypoints(gray,kp,img,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
# plt.imshow(img),plt.show()
#
# cv2.drawKeypoints(gray2,kp2,img2,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
# plt.imshow(img2),plt.show()

bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=False)
matches = bf.match(des, des2)
matches = sorted(matches, key = lambda  x:x.distance)

match_img = cv2.drawMatches(img, kp, img2, kp2, matches[:50], img, flags=2)

cv2.imwrite('sift_matches.jpg', match_img)
plt.imshow(match_img), plt.show()