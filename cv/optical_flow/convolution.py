import numpy as np
import cv2
import math as math
from scipy import signal

WINDOW_NAME = 'Frame'
INTERMEDIATE_FRAME = 'Gradient'
cap = cv2.VideoCapture('../test_video.avi')
alpha = 1
iterations = 50

def compute_derivatives(image1, image2):
    horizontal_gradient_filter = 0.25* np.matrix([[-1, 1], [-1, 1]])
#    dx1 = signal.convolve2d(image1, horizontal_gradient_filter, mode='same')
#     dx2 = signal.convolve2d(image2, horizontal_gradient_filter, mode='same')
#    dx = dx1 + dx2
    dx = cv2.filter2D(image1,-1,horizontal_gradient_filter) + cv2.filter2D(image2,-1,horizontal_gradient_filter)

    vertical_gradient_filter = 0.25*np.matrix([[-1, -1], [1, 1]])
    dy1 = signal.convolve2d(image1, vertical_gradient_filter, mode='same')
    dy2 = signal.convolve2d(image2, vertical_gradient_filter, mode='same')
    dy = dy1 + dy2

    time_derivative_filter = 0.25*np.ones([2,2])
    dt1 = signal.convolve2d(image1, time_derivative_filter, mode='same')
    dt2 = signal.convolve2d(image2, time_derivative_filter, mode='same')
    dt = dt2 - dt1

    return dx, dy, dt



kernel = np.matrix([[1, 1, 1], [1, 1, 1], [1, 1, 1]])

#image = cv2.imread('yos9.tif')
#image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

image = np.matrix([[17,   24,    1,    8,   15],
   [23,    5,    7,   14,   16], [   4,    6,   13,   20,   22],
   [10,   12,   19,   21,    3],
   [11,   18,   25,    2,    9]])
print('image sum: {}'.format(np.sum(image)))
dx1 = signal.convolve2d(image, kernel, mode='same')
print('dx1 sum: {}'.format(np.sum(dx1)))

horizontal_gradient_filter = 0.25* np.matrix([[1, 1], [1, 1]])
#horizontal_gradient_filter = 0.25* np.ones([3, 3])
dx1 = signal.convolve2d(image, horizontal_gradient_filter)

print('dx1 sum: {}'.format(np.sum(dx1)))
