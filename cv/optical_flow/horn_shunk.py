import numpy as np
import cv2
import math as math
from scipy import signal

WINDOW_NAME = 'Frame'
INTERMEDIATE_FRAME = 'Gradient'
cap = cv2.VideoCapture('../traffic.avi')
alpha = 1
iterations = 30

def compute_derivatives(image1, image2):
    horizontal_gradient_filter = 0.25* np.matrix([[-1, 1], [-1, 1]])
    dx1 = signal.convolve2d(image1, horizontal_gradient_filter, mode='same')
    dx2 = signal.convolve2d(image2, horizontal_gradient_filter, mode='same')
    dx = dx1 + dx2
    # dx = np.roll(dx, (-1,-1), axis=(0,1))

    vertical_gradient_filter = 0.25*np.matrix([[-1, -1], [1, 1]])
    dy1 = signal.convolve2d(image1, vertical_gradient_filter, mode='same')
    dy2 = signal.convolve2d(image2, vertical_gradient_filter, mode='same')
    dy = dy1 + dy2
    # dy = np.roll(dy, (-1,-1), axis=(0,1))

    time_derivative_filter = 0.25*np.ones([2,2])
    dt1 = signal.convolve2d(image1, time_derivative_filter, mode='same')
    dt2 = signal.convolve2d(image2, -time_derivative_filter, mode='same')
    dt = dt2 + dt1
    # dt = np.roll(dt, (-1,-1), axis=(0,1))

    return dx, dy, dt


def laplacian(image):
    laplacian_filter = np.matrix([[1/12, 1/6, 1/12], [1/6, 0, 1/6], [1/12, 1/6, 1/12]])
    laplacian = signal.convolve2d(image, laplacian_filter, mode='same')
    # return np.roll(laplacian, (-1,-1), axis=(0,1))
    return laplacian


def draw_optical_flow_map(image, u, v):
    step = 5

    width = image.shape[1]
    height = image.shape[0]

    for x in range(0, width, step):
        for y in range(0, height, step):

            x_flow = int(u[y,x])
            y_flow = int(v[y,x])

            x_end = x + x_flow
            y_end = y + y_flow
            end_point = (x_end, y_end)
            cv2.line(image, (x,y), end_point, (0, 255, 0), 1)
            cv2.circle(image, end_point, 1, (0,255,0))

    return image



cv2.startWindowThread()


ret, frame_1 = cap.read()
frame_1 = cv2.resize(frame_1, (0,0), fx=0.5, fy=0.5)
# frame_1 = cv2.imread('yos9.tif')
gray1 = cv2.cvtColor(frame_1, cv2.COLOR_BGR2GRAY)

u = np.zeros(gray1.shape)
v = np.zeros(gray1.shape)

while(True):
    ret, frame_2 = cap.read()
    frame_2 = cv2.resize(frame_2, (0,0), fx=0.5, fy=0.5)
    # frame_2 = cv2.imread('yos10.tif')
    gray2 = cv2.cvtColor(frame_2, cv2.COLOR_BGR2GRAY)
    cv2.imshow(WINDOW_NAME, gray2)

    [dx, dy, dt] = compute_derivatives(gray1, gray2)

    # dx = cv2.cvtColor(cv2.imread('fx.jpg'), cv2.COLOR_BGR2GRAY)
    # dy = cv2.cvtColor(cv2.imread('fy.jpg'), cv2.COLOR_BGR2GRAY)
    # dt = cv2.cvtColor(cv2.imread('ft.jpg'), cv2.COLOR_BGR2GRAY)


    # u = np.zeros(dx.shape)
    # v = np.zeros(dx.shape)
    # Averaging kernel
    kernel=np.matrix([[1/12, 1/6, 1/12],[1/6, 0, 1/6],[1/12, 1/6, 1/12]])

    for i in range(iterations):
        u_avg = laplacian(u)
        v_avg = laplacian(v)
        # u_avg = np.zeros(dx.shape)
        # v_avg = np.zeros(dx.shape)

        u_numerator = dx*(dx*u_avg + dy*v_avg + dt)
        v_numerator = dy*(dx*u_avg + dy*v_avg + dt)

        denominator = (alpha**2 + dx**2 + dy**2)

        sum_dx = np.sum(dx)
        sum_dy = np.sum(dy)
        sum_u_numerator = np.sum(u_numerator)
        sum_v_numerator = np.sum(v_numerator)
        sum_denominator = np.sum(denominator)



        # u1 = u_avg - dx*(dx*u_avg + dy*v_avg + dt) / (alpha**2 + dx**2 + dy**2)
        # v1 = v_avg - dy*(dx*u_avg + dy*v_avg + dt) / (alpha**2 + dx**2 + dy**2)
        # u_num = np.multiply((np.multiply(dx, u_avg) + np.multiply(dy, v_avg) + dt), dx)
        # v_num = np.multiply((np.multiply(dx, u_avg) + np.multiply(dy, v_avg) + dt), dy)
        # denominator = (alpha**2 + dx**2 + dy**2)
        #
        u = u_avg - np.divide(u_numerator, denominator)
        v = v_avg - np.divide(v_numerator, denominator)
        # uAvg = cv2.filter2D(u,-1,kernel)
        # vAvg = cv2.filter2D(v,-1,kernel)
        #
        # uNumer = (fx.dot(uAvg) + fy.dot(vAvg) + ft).dot(ft)
        # uDenom = alpha + fx**2 + fy**2
        # u = uAvg - np.divide(uNumer,uDenom)
        #
        # # print np.linalg.norm(u)
        #
        # vNumer = (fx.dot(uAvg) + fy.dot(vAvg) + ft).dot(ft)
        # vDenom = alpha + fx**2 + fy**2
        # v = vAvg - np.divide(vNumer,vDenom)

        

    frame_2_flow = draw_optical_flow_map(frame_2, u, v)
    cv2.imshow(INTERMEDIATE_FRAME, frame_2_flow)
    if cv2.waitKey(30) == ord('q'):
        break



    gray1 = gray2

cv2.destroyAllWindows()
cap.release()
