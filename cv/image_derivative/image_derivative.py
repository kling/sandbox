import cv2
import numpy as np
from scipy import signal

filename = '../test_image.jpg'
image = cv2.imread(filename)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

horizontal_gradient_filter = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
horizontal_gradient = signal.convolve2d(gray, horizontal_gradient_filter)
cv2.imwrite('horizontal_gradient.png', horizontal_gradient)

vertical_gradient_filter = np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])
vertical_gradient = signal.convolve2d(gray, vertical_gradient_filter)
cv2.imwrite('vertical_gradient.png', vertical_gradient)
