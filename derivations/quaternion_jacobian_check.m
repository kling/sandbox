clear all
clc

pkg load symbolic;

syms qw qx qy qz;
q = [qw; qx; qy; qz];
v = q(2:4);

%R_11 = 1 - 2*(qy^2 + qz^2);
R_11 = qw^2 + qx^2 - qy^2 - qz^2;
R_12 = 2*(qx*qy - qz*qw);
R_13 = 2*(qx*qz + qy*qw);
R_21 = 2*(qx*qy + qz*qw);
%R_22 = 1 - 2*(qx^2 + qz^2);
R_22 = qw^2 - qx^2 + qy^2 - qz^2;
R_23 = 2*(qy*qz - qx*qw);
R_31 = 2*(qx*qz - qy*qw);
R_32 = 2*(qy*qz + qx*qw);
%R_33 = 1 - 2*(qx^2 + qy^2);
R_33 = qw^2 - qx^2 - qy^2 + qz^2;

R = [R_11, R_12, R_13; ...
     R_21, R_22, R_23; ...
     R_31, R_32, R_33];
     
syms a1 a2 a3;
a = [a1;a2;a3];

Ra = R*a;

dRa_dqw = diff(Ra, qw);
dRa_dqx = diff(Ra, qx);
dRa_dqy = diff(Ra, qy);
dRa_dqz = diff(Ra, qz);
dRa_dq = [dRa_dqw, dRa_dqx, dRa_dqy, dRa_dqz]


% Equation 174 From Joan Sola's Quaternion Kinematics of the Error State Kalman Filter.
left_columm = qw*a + cross(v,a)

skew_symmetric_a = [0, -a3, a2; ...
                    a3, 0, -a1; ...
                    -a2, a1, 0];
right_submatrix = v'*a*eye(3) + v*a' - a*v' - qw*skew_symmetric_a;

dqaq_dq = 2*[left_columm, right_submatrix]

dRa_dq - dqaq_dq


