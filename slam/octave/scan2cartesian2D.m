% Converts a 2D laser scan to a set of cartsian points in the lidar frame.
function [pos_xy] = scan2cartesian2D (num_points, scan_ranges, min_angle, angle_increment, max_range)
  assert(num_points == length(scan_ranges), 'List of scan ranges has an incorrect length.');

  angles = min_angle + [0:num_points-1] .* angle_increment;

  % Only keep measurements that are in range.
  valid_indexes = find(scan_ranges < max_range);
  scan_ranges = scan_ranges(valid_indexes);
  angles = angles(valid_indexes);
  
  pos_x = cos(angles) .* scan_ranges;
  pos_y = sin(angles) .* scan_ranges;
  pos_xy = [pos_x', pos_y'];
endfunction
