## Copyright (C) 2018 Kevin
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} rotate2D (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Kevin <kevin@kevin-linux>
## Created: 2018-12-07

function [out_xy] = rotate2D (pos_xy, angle)
  R = [cos(angle), -sin(angle); ...
      sin(angle), cos(angle)];
  out_xy = (R * pos_xy')';

endfunction
