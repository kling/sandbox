#include <gtest/gtest.h>

namespace {
TEST(SimpleTest, AlwaysPass) {
  EXPECT_TRUE(true);
}

TEST(SimpleTest, AlwaysFail) {
  EXPECT_TRUE(false);
}
} // namespace

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  RUN_ALL_TESTS();
  return 0;
}


