#include <gtest/gtest.h>

#include "kdtree.h"

namespace {

using FloatPoint = KdTreePoint<float>;

void ExpectPointsEqual(const FloatPoint& expected, const FloatPoint& actual) {
  EXPECT_EQ(expected.x_, actual.x_);
  EXPECT_EQ(expected.y_, actual.y_);
  EXPECT_EQ(expected, actual);
}

TEST(KdTreePoint, EqualityOperator) {
  FloatPoint left(3.0f, 4.0f);
  FloatPoint right(3.0f, 4.0f);
  EXPECT_EQ(left, right);
}

TEST(KdTreePoint, SubtractionOperator) {
  FloatPoint left(3.0f, 4.0f);
  FloatPoint right(2.0f, 2.0f);
  EXPECT_EQ(FloatPoint(1.0f, 2.0f),  left - right);
}

TEST(KdTreePoint, TwoNorm) {
  FloatPoint p(3.0f, 4.0f);
  EXPECT_DOUBLE_EQ(5.0,  p.TwoNorm());
}


class KdTreeTester : public testing::Test {
 protected:
  void SetUp() override {
    kdtree_ = new KdTree<float>();
  }

  virtual void TearDown() {
    delete kdtree_;
  }

  KdTree<float>* kdtree_;
};

TEST_F(KdTreeTester, FindNearestIsNullWhenEmpty) {
  FloatPoint point(2.0f, 3.2f);
  std::shared_ptr<FloatPoint> nearest = kdtree_->FindNearest(point);
  EXPECT_EQ(NULL, nearest.get());
}

TEST_F(KdTreeTester, FindNearestWithOneInsert) {
  FloatPoint point(2.0f, 3.2f);
  kdtree_->Insert(point.x_, point.y_);
  
  std::shared_ptr<FloatPoint> nearest = kdtree_->FindNearest(point);
  EXPECT_EQ(point, *nearest);
}

TEST_F(KdTreeTester, FindNearestFirstOfTwoInserts) {
  FloatPoint point(2.0f, 3.2f);
  FloatPoint point2(4.0f, 5.2f);
  kdtree_->Insert(point.x_, point.y_);
  kdtree_->Insert(point2.x_, point2.y_);
  
  std::shared_ptr<FloatPoint> nearest = kdtree_->FindNearest(point);
  ExpectPointsEqual(point, *nearest);
}

TEST_F(KdTreeTester, FindNearestSecondOfTwoInserts) {
  FloatPoint point(2.0f, 3.2f);
  FloatPoint point2(4.0f, 5.2f);
  kdtree_->Insert(point.x_, point.y_);
  kdtree_->Insert(point2.x_, point2.y_);
  
  std::shared_ptr<FloatPoint> nearest = kdtree_->FindNearest(point2);
  ExpectPointsEqual(point2, *nearest);
}

TEST_F(KdTreeTester, FindNearestOfThreeNoExact) {
  FloatPoint point(2.0f, 3.2f);
  FloatPoint point2(4.0f, 5.2f);
  FloatPoint point3(1.0f, 4.2f);
  kdtree_->Insert(point.x_, point.y_);
  kdtree_->Insert(point2.x_, point2.y_);
  kdtree_->Insert(point3.x_, point3.y_);
  
  FloatPoint target(0.5, 4.1);
  std::shared_ptr<FloatPoint> nearest = kdtree_->FindNearest(target);
  ExpectPointsEqual(point3, *nearest);
}
