#pragma once

#include <vector>
#include <memory>
#include <math.h>
#include <limits>

template <typename T>
class KdTreePoint {
 public:
  KdTreePoint(T x, T y) : x_(x), y_(y) { }
  T x_;
  T y_;

  double TwoNorm() {
    return sqrt(x_*x_ + y_*y_);
  }

};

template <typename T>
bool operator== (const KdTreePoint<T> lhs, const KdTreePoint<T> rhs) {
  return lhs.x_ == rhs.x_ && lhs.y_ == rhs.y_;
}

template <typename T>
KdTreePoint<T> operator- (const KdTreePoint<T> lhs, const KdTreePoint<T> rhs) {
  return KdTreePoint<T>(lhs.x_ - rhs.x_, lhs.y_ - rhs.y_);
}

template <typename T>
class KdTreeNode {
 public:
  KdTreeNode(T x, T y) { point_ = std::make_shared<KdTreePoint<T>>(x, y); }
  KdTreeNode(std::shared_ptr<KdTreePoint<T>> point) : point_(point) {}

  std::shared_ptr<KdTreePoint<T>> point_;
  std::shared_ptr<KdTreeNode> left_ = NULL;
  std::shared_ptr<KdTreeNode> right_ = NULL;
};

// 2D K-d Tree
template <typename T>
class KdTree {
 public:
  using Node = KdTreeNode<T>;
  using Point = KdTreePoint<T>;

  KdTree() = default;

  void Insert(T x, T y);

  // Builds the KD-Tree with the given array of points. This will delete the
  // current tree if it already has elements.
  void Build(std::shared_ptr<std::vector<Point>> points);

  std::shared_ptr<Point> FindNearest(const Point& point);

 private:
  std::shared_ptr<Node> root_ = NULL;

  std::shared_ptr<Point> FindNearest(const Point& point,
      std::shared_ptr<Node> node,
      int depth,
      std::shared_ptr<Point> nearest,
      double best_distance);

  void Insert(std::shared_ptr<Point> point,
              std::shared_ptr<Node> node,
              int depth);
};

template <typename T>
std::shared_ptr<KdTreePoint<T>> KdTree<T>::FindNearest(
    const KdTreePoint<T>& point) {
  if (!root_) {
    return NULL;
  }
  return FindNearest(point, root_, 0, NULL,
      std::numeric_limits<double>::max());
}

// Node is guaranteed to not be null.
template <typename T>
std::shared_ptr<KdTreePoint<T>> KdTree<T>::FindNearest(
    const KdTreePoint<T>& point,
    std::shared_ptr<KdTreeNode<T>> node,
    int depth,
    std::shared_ptr<KdTreePoint<T>> nearest,
    double best_distance) {

  if (point == *(node->point_)) {
    return node->point_;
  }

  double current_distance = (*node->point_ - point).TwoNorm();
  if (current_distance < best_distance) {
    best_distance = current_distance;
    nearest = node->point_;
  }

  // TODO: Handle NULL children
  if (depth % 2) {
    if (point.y_ >= node->point_->y_) {
      if (node->right_) {
        return FindNearest(point, node->right_, depth+1, nearest, best_distance);
      } else {
        return nearest;
      }
    } else {
      if (node->left_) {
        return FindNearest(point, node->left_, depth+1, nearest, best_distance);
      } else {
        return nearest;
      }
    }
  } else {
    if (point.x_ >= node->point_->x_) {
      if (node->right_) {
        return FindNearest(point, node->right_, depth+1, nearest, best_distance);
      } else {
        return nearest;
      }
    } else {
      if (node->left_) {
        return FindNearest(point, node->left_, depth+1, nearest, best_distance);
      } else {
        return nearest;
      }
    }
  }
}

template <typename T>
void KdTree<T>::Insert(T x, T y) {
  if (!root_) {
    root_ = std::make_shared<Node>(x, y);
  } else {
    auto point = std::make_shared<Point>(x, y);
    Insert(point, root_, 0);
  }
}

template <typename T>
void KdTree<T>::Insert(std::shared_ptr<Point> point,
                       std::shared_ptr<Node> node,
                       int depth) {
  bool split_on_y_axis = depth % 2;
  if (split_on_y_axis) {
    if (point->y_ >= node->point_->y_) {
      // Insert or explore right.
      if (node->right_) {
        Insert(point, node->right_, depth+1);
      } else {
        node->right_ = std::make_shared<KdTreeNode<T>>(point);
      }
    } else {
      if (node->left_) {
        Insert(point, node->left_, depth+1);
      } else {
        node->left_ = std::make_shared<KdTreeNode<T>>(point);
      }
    }
  } else {
    if (point->x_ >= node->point_->x_) {
      // Insert or explore right.
      if (node->right_) {
        Insert(point, node->right_, depth+1);
      } else {
        node->right_ = std::make_shared<KdTreeNode<T>>(point);
      }
    } else {
      if (node->left_) {
        Insert(point, node->left_, depth+1);
      } else {
        node->left_ = std::make_shared<KdTreeNode<T>>(point);
      }
    }
  }
}

template <typename T>
void KdTree<T>::Build(std::array<Point> point,
                      std::shared_ptr<Node> node,
                      int depth) {
}
