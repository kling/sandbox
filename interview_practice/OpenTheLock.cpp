#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    
    void turnKnob(string& s, int knob, bool up) {
        if (up) {
            if (s[knob] == '9') {
                s[knob] = '0';
            } else {
                s[knob]++;
            }
        } else {
            if (s[knob] == '0') {
                s[knob] = '9';
            } else {
                s[knob]--;
            }
        }
    }
    
    int solve(unordered_set<string>& deadends, string start, string& target) {
        int min_solution = numeric_limits<int>::max();
        if (start == target) { return 0; }
        if (deadends.count(start)) return -1;
        
        string initial_string = start;
        
        for (int i = 0; i < 4; i++) {
            turnKnob(start, i, true);
            if (deadends.find(start) == deadends.end()) {
                deadends.insert(start);
                int knob_up = solve(deadends, start, target);
                if (knob_up >= 0) {
                    min_solution = min(min_solution, 1 + knob_up);    
                }
            }
            
            
            
            start = initial_string;
            
            turnKnob(start, i, false);
            if (deadends.find(start) == deadends.end()) {
                deadends.insert(start);
                int knob_down = solve(deadends, start, target);
                // if (knob_down != numeric_limits<int>::max()) return 1 + knob_down;
                if (knob_down >= 0) {
                    min_solution = min(min_solution, 1 + knob_down);    
                }
            }
        }


        if (min_solution == numeric_limits<int>::max()) {
            return -1;
        }        
        return min_solution;
    }
    
    int openLock(vector<string>& deadends, string target) {
        unordered_set<string> deadend_map;
        for (auto s: deadends) {
            deadend_map.insert(s);
        }
        string start("0000");
        return solve(deadend_map, start, target);
    }
};

int main() {
    Solution sol;
    vector<string> deadends = {string("8888")};
    string target("0009");
    int turns = sol.openLock(deadends, target);

    cout << "Solution: " << turns << endl;
}