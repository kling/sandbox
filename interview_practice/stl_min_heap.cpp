#include <bits/stdc++.h> 
#include <iostream>

using namespace std;

typedef struct {
    int spent;
    int num_purchases;
} Person;

class PersonComparator {
public:
    bool operator() (const Person& p1, const Person& p2) {
        return p1.spent > p2.spent;
    }
};

int customComparator () {
  priority_queue<Person, vector<Person>, PersonComparator> min_heap;
  for (int i = 0; i < 5; i++) {
    Person p = {.spent = i};
    min_heap.push(p);
  }

  cout << "Person min heap: ";
  while (min_heap.size()) {
    Person p = min_heap.top();
    min_heap.pop();
    cout << p.spent << ", ";
  }
  cout << endl;
}

int main() {
  priority_queue<int> max_heap;
  max_heap.push(5);
  max_heap.push(2);
  max_heap.push(7);
  max_heap.push(0);

  cout << "max heap: ";
  int heap_size = max_heap.size();
  for (int i = 0; i < heap_size; i++) {zs
    cout << max_heap.top() << ", ";
    max_heap.pop();
  }
  cout << endl;


  priority_queue<int, vector<int>, greater<int>> min_heap;
  min_heap.push(5);
  min_heap.push(2);
  min_heap.push(7);
  min_heap.push(0);

  cout << "max heap: ";
  heap_size = min_heap.size();
  for (int i = 0; i < heap_size; i++) {
    cout << min_heap.top() << ", ";
    min_heap.pop();
  }
  cout << endl;

  customComparator();
}