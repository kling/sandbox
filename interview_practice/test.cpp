#include <cmath>
#include <cstdio>
#include <vector>
#include <list>
#include <map>
#include <iostream>
#include <algorithm>
using namespace std;

class Graph {
    public:
        typedef struct {
            int id;
            int total_distance;
        } NodeToVisit;

        int num_nodes;
        std::vector<std::list<int> > connections;
        map<int, int> distances;

        Graph(int n) : connections(n) {
            num_nodes = n;
            
            for (int i = 1; i <= n; i++) {
                distances[i] = -1;
            }
        }
    
        void add_edge(int u, int v) {
            cout << "Adding edge " << u << ", " << v << endl;

            list<int>& l1 = connections[u-1];
            l1.push_back(v);

            
            list<int>& l2 = connections[v-1];
            l2.push_back(u);
            cout << "Done" << endl;
        }
};

int main() {
  std::vector<list<int> > t(5);
  t[0].push_back(5);
  t[0].push_back(4);
  t[0].push_back(3);
  t[1].push_back(2);
  std::cout << "test: " << t[0].front() << endl;
  std::cout << "test: " << t[1].front() << endl;


  Graph graph(5);
  graph.add_edge(0, 1);
  cout << "Done program." << endl;

  return 0;
}


// #include <cmath>
// #include <cstdio>
// #include <vector>
// #include <list>
// #include <map>
// #include <iostream>
// #include <algorithm>
// using namespace std;

// class Graph {
//     public:
//         typedef struct {
//             int id;
//             int total_distance;
//         } NodeToVisit;

//         int num_nodes;
//         vector<list<int> > connections;
//         map<int, int> distances;

//         Graph(int n) : connections(n, list<int>()) {
//             num_nodes = n;
            
//             for (int i = 1; i <= n; i++) {
//                 distances[i] = -1;
//             }
//         }
    
//         void add_edge(int u, int v) {
//             cout << "Adding edge " << u << ", " << v << endl;

//             list<int>& l1 = connections[u-1];
//             l1.push_back(v);

            
//             list<int>& l2 = connections[v-1];
//             l2.push_back(u);
//             cout << "Done" << endl;
//         }
    
//         vector<int> shortest_reach(int start) {
//             list<NodeToVisit> queue;
//             NodeToVisit start_node;
//             start_node.id = start;
//             start_node.total_distance = 0;
//             queue.push_back(start_node);
            
//             while (queue.size()) {
//                 NodeToVisit curr = queue.front();
//                 queue.pop_front();

//                 distances[curr.id] = curr.total_distance + 6;
//                 for (auto neighbor_id : connections[curr.id - 1]) {
//                     // Only add unvisited neighbors.
//                     if (distances[neighbor_id] == -1) {
//                         NodeToVisit to_queue;
//                         to_queue.id = neighbor_id;
//                         to_queue.total_distance = curr.total_distance + 6;
//                     }
//                 }
//             }

//             vector<int> shortest_reach_distances(num_nodes);
//             for (int i = 1; i <= num_nodes; i++) {
//                 shortest_reach_distances[i-1] = distances[i];
//             }
//             return shortest_reach_distances;
//         }
    
// };

// int main() {
//   Graph graph(5);
//   graph.add_edge(0, 1);
//   cout << "Done program." << endl;

//   // std::vector<list<int> > t(5, list<int>());
//   // t[0].push_back(5);
//   // t[0].push_back(4);
//   // t[0].push_back(3);

//   // t[1].push_back(2);

//   // std::cout << "test: " << t[0].front() << endl;
//   // std::cout << "test: " << t[1].front() << endl;

//   return 0;
// }