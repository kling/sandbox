#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    int sumSubarray(vector<int>& nums, map<pair<int, int>, int>& substr_sums, pair<int, int>& bounds) {
//         if (substr_sums.find(bounds) != substr_sums.end()) {
//             return substr_sums[bounds];
//         } else {
//             pair<int, int> prev_bound(bounds);
//             if (prev_bound.second > prev_bound.first) {
//                 prev_bound.second--;
                
//                 if (substr_sums.find(prev_bound) != substr_sums.end()) {
//                     return substr_sums[prev_bound] + nums[bounds.second];   
//                 }
//             }
//         }
        
        int sum = 0;
        for (int i = bounds.first; i < bounds.second+1; i++) {
            sum += nums[i];
        }
        
        cout << "Sum func: " << sum <<endl;
        return sum;
    }
    
    bool checkSubarraySum(vector<int>& nums, int k) {
        map<pair<int, int>, int> substr_sums;
        
        for (int i = 0; i < nums.size(); i++) {
            int len_lower_bound = 0;
            // if (k != 0) len_lower_bound = nums[i] / k;
            
            pair<int, int> bounds;
            bounds.first = i;
            for (int j = i; j < nums.size() - len_lower_bound; j++) {
                bounds.second = j;
                
                int sum = sumSubarray(nums, substr_sums, bounds);
                cout << "Sum[" << bounds.first << ", " <<bounds.second << "]: " << sum <<endl;
                
                
                if (sum == k * (j-i + 1)) return true;
                
                // Update j to new lower bound.
            }
        }
        return false;
    }
};

int main() {
    vector<int> test_data = {23, 2, 4, 6, 7};
    int k = 6;
    Solution sol;


    if (sol.checkSubarraySum(test_data, k)) {
        cout << "Yes" <<endl;
    } else {
        cout << "No" << endl;
    }

    return 0;
}