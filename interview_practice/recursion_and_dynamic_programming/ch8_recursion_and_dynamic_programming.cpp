#include <vector>
#include <utility>
#include <iostream>
#include <list>
#include <map>
#include <stdlib.h>

//8.1 Triple step
int triple_step(int n) {
  if (n < 0) {
    return 0;
  } else if (n == 0) {
    return 1;
  }

  return triple_step(n-3) + triple_step(n-2) + triple_step(n-1);
}

int count_ways_memoized(int n, std::vector<int>& cache) {
  if (n < 0) {
    return 0;
  } else if (n == 0) {
    return 1;
  }

  if (cache[n] != -1) {
    return cache[n];
  }

  cache[n] = count_ways_memoized(n-1, cache) + count_ways_memoized(n-2, cache) +
      count_ways_memoized(n-3, cache);
  return cache[n];
}

int triple_step_memoized(int n) {
  std::vector<int> cache(n+1);
  for (auto it = cache.begin(); it != cache.end(); it++) {
    *it = -1;
  }
  return count_ways_memoized(n, cache);
}

typedef struct {
  int row;
  int column;
} Point;

bool get_path(bool** matrix, int r, int c, std::list<Point>& path) {
  if (r < 0 || c < 0 || !matrix[r][c]) return false;

  bool at_origin = r == 0 && c == 0;

  if (at_origin || 
      get_path(matrix, r-1, c, path) ||
      get_path(matrix, r, c-1, path)) {

    Point p;
    p.row = r;
    p.column = c;
    path.push_back(p);
    return true;
  }

  return false;
}


int magic_index_binary_search(std::vector<int>& sorted_array, int start, int end) {
  if (start > end) {
    return -1;
  }

  int mid = (start + end) / 2;

  if (sorted_array[mid] == mid) {
    return mid;
  }

  if (sorted_array[mid] < mid) {
    return magic_index_binary_search(sorted_array, mid+1, end);
  } else {
    return magic_index_binary_search(sorted_array, start, mid-1);
  }
}


int magic_index(std::vector<int>& sorted_array) {
  int high = sorted_array.size() - 1;
  int low = 0;

  return magic_index_binary_search(sorted_array, low, high);
}

int magic_index_not_distinct(int sorted_array[], int start, int end) {
  if (end < start) {
    return -1;
  }

  int min = sorted_array[start];
  int max = sorted_array[end];

  if (sorted_array[start] == start) {
    return start;
  }
  if (sorted_array[end] == end) {
    return end;
  }

  int next_start = start+1;
  if (min >= 0) { next_start = min; }
  int next_end = end-1;
  if (max < end) { next_end = max; }
  return magic_index_not_distinct(sorted_array, next_start, next_end);
}


void print_list(std::list<int>& list) {
  for (auto& i : list) {
    std::cout << i << ", ";
  }
  std::cout << std::endl;
}

void print_list(std::list<char>& list) {
  for (auto& c : list) {
    std::cout << c;
  }
  std::cout << std::endl;
}

void power_sets_no_duplicates(std::vector<int> set, std::list<int> subset) {
  if (set.empty()) {
    print_list(subset);
    return;
  }

  int back = set.back();
  set.pop_back();

  power_sets_no_duplicates(set, subset);

  subset.push_back(back);
  power_sets_no_duplicates(set, subset);
}

// Prints all subsets of a set. Assumes there are no duplicates.
void power_sets_no_duplicates(std::vector<int> set) {
  std::list<int> subset;
  power_sets_no_duplicates(set, subset);
}

void hanoi_print_state(std::list<int>& left, std::list<int>& mid, std::list<int>& right) {
  std::cout << "Left: ";
  print_list(left);
  std::cout << "Mid: ";
  print_list(mid);
  std::cout << "Right: ";
  print_list(right);
}

void towers_of_hanoi(int n) {
  std::list<int> left;
  std::list<int> mid;
  std::list<int> right;

  for (int i = n; i > 0; i--) {
    left.push_back(i);
  }

  hanoi_print_state(left, mid, right);
}

void print_permutations(std::vector<char> set, std::list<char> sequence, int index) {
  sequence.push_back(set[index]);
  set.erase(set.begin() + index);
  if (set.empty()) {
    print_list(sequence);
    return;
  }

  for (int i = 0; i < set.size(); i++) {
    print_permutations(set, sequence, i);
  }
}

std::vector<char> string_to_vector(std::string& s) {
  std::vector<char> v;
  for (auto& c : s) {
    v.push_back(c);
  }
  return v;
}

void print_permutations_no_duplicates(std::string s) {
  std::vector<char> set = string_to_vector(s);

  std::list<char> prefix;

  for (int i = 0; i < set.size(); i++) {
    print_permutations(set, prefix, i);
  }
}

void permute_character_map(std::map<char, int> map, std::list<char> prefix, int char_remaining) {
  if (char_remaining == 0) {
    print_list(prefix);
    return;
  }

  for (auto& it : map) {
    char c = it.first;

    if (map[c] > 0) {
      prefix.push_back(c);
      map[c] -= 1;
      permute_character_map(map, prefix, char_remaining-1);
      map[c] += 1;
      prefix.pop_back();
    }
  }
}

void print_permutations_with_duplicates(std::string s) {
  std::map<char, int> set;
  for (auto& c : s) {
    if (!set.count(c)) {
      set[c] = 0;
    }
    set[c] += 1;
  }

  std::list<char> prefix;
  permute_character_map(set, prefix, s.length());
}

void parens_helper(int pairs, int close, std::list<char> prefix) {
  if (!pairs && !close) {
    print_list(prefix);
    return;
  }

  if (pairs) {
    prefix.push_back('(');
    parens_helper(pairs-1, close+1, prefix);
    prefix.pop_back();
  }
  if (close) {
    prefix.push_back(')');
    parens_helper(pairs, close-1, prefix);
    prefix.pop_back();
  }
}

// Print all possible valid combinations for n parenthesis pairs.
void parens(int n) {
  int remaining_pairs = n;
  int remaining_close = 0;
  std::list<char> prefix;

  parens_helper(remaining_pairs, remaining_close, prefix);
}

int coins_helper_pennies(int cents_remaining) {
  return 1;
}

int coins_helper_nickles(int cents_remaining) {
  if (cents_remaining < 5) {
    return 1;
  }

  return (cents_remaining / 5) + 1;
}

int coins_helper_dimes(int cents_remaining) {
  if (cents_remaining == 0) {
    return 1;
  }

  int combination_count = 0;
  int i;
  for (i = 0; i < (cents_remaining / 10) + 1; i++) {
    combination_count += coins_helper_nickles(cents_remaining - i*10);
  }
  return combination_count + i - 1;
}

int coins_helper_quarters(int cents_remaining) {
  int combination_count = 0;
  int i;
  for (i = 0; i < (cents_remaining / 25) + 1; i++) {
    combination_count += coins_helper_dimes(cents_remaining - i*25);
  }
  return combination_count + i - 1;
}

// Returns the number of ways to represent n_cents. This will cascade through functions that dish
// out every possible combination of each coin.
int coins(int n_cents) {
  if (n_cents == 0) {
    return 1;
  }

  return coins_helper_quarters(n_cents);
}

void eight_queens_print_map(std::list<std::pair<int, int> >& map) {
  std::cout << "Map Start" << std::endl;
  for (auto& p : map) {
    std::cout << "Queen: " << p.first << ", " << p.second << std::endl;
  }
  std::cout << "Map End" << std::endl;
}

// Returns true if the new queen placement is valid.
bool eight_queens_valid_placement(int row, int col, std::list<std::pair<int, int> >& map) {
  for (auto& pair : map) {
    int existing_col = pair.first;
    int existing_row = pair.second;


    int col_diff = std::abs(col - existing_col);
    int row_diff = std::abs(row - existing_row);
    
    bool col_collision = col == existing_col;
    bool diagonal = col_diff == row_diff;
    if (col_collision || diagonal) {
      return false;
    }
  }
  return true;
}

void eight_queens_permute_columns(std::vector<int> columns_remaining, std::list<std::pair<int, int> > map, int row) {
  if (!columns_remaining.size()) {
    eight_queens_print_map(map);

    std::cout << "Printing Map" <<std::endl;
  }

  for (int i = 0; i < columns_remaining.size(); i++) {
    int col = columns_remaining[i];

    if (eight_queens_valid_placement(row, col, map)) {
      std::pair<int, int> p(col, row);
      map.push_back(p);

      std::vector<int> copy_col_rem = columns_remaining;
      copy_col_rem.erase(copy_col_rem.begin() + i);
      eight_queens_permute_columns(copy_col_rem, map, row+1);

      map.pop_back();
    }
    
  }
}


void eight_queens(int map_size) {
  std::list<std::pair<int, int> > map;

  std::vector<int> v;
  for (int i = 0; i < map_size; i++) {
    v.push_back(i);
  }

  eight_queens_permute_columns(v, map, 0);
}

int main() {
  //std::cout << triple_step(30) << std::endl;
  //std::cout << triple_step_memoized(30) << std::endl;

  

  // magic index
  // std::vector<int> sorted_no_duplicates;
  // sorted_no_duplicates.push_back(-10);
  // sorted_no_duplicates.push_back(-5);
  // sorted_no_duplicates.push_back(-1);
  // sorted_no_duplicates.push_back(0);
  // sorted_no_duplicates.push_back(4);
  // sorted_no_duplicates.push_back(6);
  // sorted_no_duplicates.push_back(8);
  // sorted_no_duplicates.push_back(9);
  // sorted_no_duplicates.push_back(12);
  // sorted_no_duplicates.push_back(15);
  // std::cout << "Magic index: " << magic_index(sorted_no_duplicates) << std::endl;


  // int sorted_with_duplicates[] = {-10, 5, 5, 5, 7, 8, 9, 9, 9, 10, 11, 12, 14, 15, 15, 20};
  // std::cout << "Magic index with duplicates: " << magic_index_not_distinct(sorted_with_duplicates, 0, 16) << std::endl;


  // int array_set[] = {1, 2, 3, 4, 5};
  // std::vector<int> set(array_set, array_set + sizeof(array_set) / sizeof(int));
  // power_sets_no_duplicates(set);

  // print_permutations_no_duplicates(std::string("aabc"));
  // print_permutations_with_duplicates(std::string("aacb"));

  // towers_of_hanoi(3);

  // parens(4);
  int cents = 100;
  std::cout << "Combinations to make " << cents << " cents: " << coins(cents) << std::endl;

  eight_queens(5);

  return 0;
}

