import copy

# Questions from Cracking the Coding Interview 6th edition

# Question 1.1, returns true if all characters are unique.
def is_unique(string):
    dict = {}
    for c in string:
        if c in dict:
            return False
        else:
            dict[c] = 1
    return True


# Question 1.2, Returns true if one string is a permutation of the other.
def is_permutation(a, b):
    d = dict()
    for c in a:
        if c in d:
            d[c] += 1
        else:
            d[c] = 1

    for c in b:
        if c in d:
            d[c] -= 1
            if d[c] is 0:
                d.pop(c, None)
        else:
            return False
    if len(d.keys()) is not 0:
        return False
    return True

# Question 1.3 done in C++

# Question 1.4: Palindrome permutation. Return true if the input is a permutation of a palindrome
def is_permutation_of_palindrome(string):
    d = {}
    for c in string:
        if c in d:
            d[c] += 1
        else:
            d[c] = 1

    num_odd = 0
    for k in d.keys():
        if d[k] % 2 is not 0:
            num_odd += 1

    if len(string) % 2 is 0:
        if num_odd is not 0:
            return False
    else:
        return num_odd is 1


# Question 1.5: Return true if one string is one edit away from the other or an exact match.
def is_one_away(a, b):
    a_len = len(a)
    b_len = len(b)
    if abs(a_len - b_len) > 1:
        return False

    differences = 0
    i = 0
    j = 0
    while i < a_len and j < b_len:
        if a[i] == b[j]:
            i += 1
            j += 1
        elif a_len > b_len:
            i += 1
            differences += 1
        elif b_len > a_len:
            j += 1
            differences += 1
        else:
            differences += 1
            i += 1
            j += 1

    return differences <= 1

# Question 1.6: String compression
def compress_string(string):
    compressed = ''
    repeat_counter = 0
    for i in range(len(string) - 1):
        if string[i] is string[i+1]:
            repeat_counter += 1
        else:
            if repeat_counter is not 0:
                compressed += '{}{}'.format(string[i], repeat_counter + 1)
                repeat_counter = 0
            else:
                compressed += string[i]

    if repeat_counter is not 0:
        compressed += '{}{}'.format(string[i], repeat_counter + 1)
        repeat_counter = 0
    else:
        compressed += string[i]

    if len(compressed) < len(string):
        return compressed
    else:
        return string

# Question 1.7: Rotate MAtrix
def rotate_matrix(matrix, n):
    for i in range(0, n, 2):
        for j in range(i, n - 2*i):
            temp = matrix[i][j]
            matrix[i][j] = matrix[n-1-i-j][i]
            matrix[n-1-i-j][i] = matrix[n-1-i][n-1-i-j]
            matrix[n - 1 - i][n - 1 - i-j] = matrix[j][n-1-i]
            matrix[j][n-1-i] = temp

    return matrix

# Question 1.8: Zero Matrix
def zero_matrix(matrix, m, n):
    out_matrix = copy.deepcopy(matrix)

    for i in range(m):
        for j in range(n):
            if matrix[i][j] is 0:
                # Zero row
                for k in range(m):
                    out_matrix[k][j] = 0

                for l in range(n):
                    out_matrix[i][l] = 0
    return out_matrix

def print_matrix(matrix, m, n):
    line = ''
    print('')
    for i in range(m):
        for j in range(n):
            line += '\t{}'.format(matrix[i][j])
        print(line)
        line = ''

# Question 1.9: String Rotation


if __name__ == '__main__':
    print('Q1.1: Is unique')
    print('String abcdefghij is: {}'.format(is_unique('abcdefghij')))
    print('String abcdefghija is: {}'.format(is_unique('abcdefghija')))
    print('')

    print('Q1.2: Is permutation')
    print('String abcde and bcdef: {}'.format(is_permutation('abcde', 'bcdef')))
    print('String deadbeef and feedbead: {}'.format(is_permutation('deadbeef', 'feedbead')))
    print('')

    print('Q1.4: Is permutation of palindrome')
    print('String abcde: {}'.format(is_permutation_of_palindrome('abcde')))
    print('String arcearc (racecar): {}'.format(is_permutation_of_palindrome('arcearc')))
    print('')

    print('Q1.5: Is One Away')
    print('abcde and fghij: {}'.format(is_one_away('abcde', 'fghij')))
    print('abcde and bcde: {}'.format(is_one_away('abcde', 'bcde')))
    print('abcde and abcdf: {}'.format(is_one_away('abcde', 'abcdf')))
    print('abe and abc: {}'.format(is_one_away('abe', 'abc')))

    print('Q1.6: Compress stsring')
    print('aabbccdd: {}'.format(compress_string('aabbccdd')))
    print('aaabbbcccaaa: {}'.format(compress_string('aaabbbcccaaa')))

    print('Q1.7: Rotate Matrix')
    matrix = [[1,2,3,4],[5,6,7,8], [9,10,11,12], [13,14,15,16]]
    print_matrix(matrix, 4, 4)
    r_matrix = rotate_matrix(matrix, 4)
    print_matrix(r_matrix, 4, 4)

    print('Q1.8: zero Matrix')
    matrix = [[1,2,3,4],[5,6,7,8], [9,0,11,12], [13,14,15,16]]
    print_matrix(matrix, 4, 4)
    r_matrix = zero_matrix(matrix, 4, 4)
    print_matrix(r_matrix, 4, 4)