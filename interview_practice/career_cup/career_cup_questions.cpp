#include <iostream>
#include <vector>

// https://www.careercup.com/question?id=5716403849003008
//
// Given an array of integers greater than zero, find if it is possible 
// to split it in two (without reordering the elements), such that the 
// sum of the two resulting arrays is the same. Print the resulting arrays.

bool arrayPartition(std::vector<int> nums) {
  int a = 0;
  int b = nums.size();
  int sumA = 0;
  int sumB = 0;

  while (a!= b) {
    if (sumA < sumB) {
      sumA += nums[a];
      a++;
    } else {
      b--;
      sumB += nums[b];
    }
  }

  if (sumA == sumB) {
    std::cout << "True: " << a << std::endl;
  } else {
    std::cout << "False" << std::endl;
  }

  return sumA == sumB;
}

int main() {
  std::vector<int> nums = {8, 2, 2, 2, 2, 4, 4};

  arrayPartition(nums);
  return 0;
}
