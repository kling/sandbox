#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    bool is24(float v) {
        return abs(v - 24) < 1e-4;
    }
    
    void removeValue(vector<float>& nums, int v) {
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] == v) {
                nums.erase(nums.begin()+i);
                return;
            }
        }
    }
    
    bool solve(vector<float>& nums) {
        if (nums.size() == 0) return false;
        if (nums.size() == 1) return is24(nums[0]);
        
        for (int i = 0; i < nums.size(); i++) {
            for (int j = 0; j < nums.size(); j++) {
                if (i == j) continue;
                vector<float> curr;

                for (int k = 0; k < nums.size(); k++) {
                    if (k == i || k == j) continue;
                    curr.push_back(nums[k]);
                }
                
                for (int k = 0; k < 4; k++) {
                    if (k < 2 && j > i) continue;
                    float result = 0;
                    bool skip = false;
                    switch (k) {
                        case 0:
                            result = nums[i] + nums[j];
                            break;
                        case 1:
                            result = nums[i] * nums[j];
                            break;
                        case 2:
                            result = nums[i] - nums[j];
                            break;
                        case 3:
                            if (nums[j] != 0) {
                                result = nums[i] / nums[j];
                            } else {
                                skip == true;
                            }
                            break;
                        default:
                            result = 0;
                            break;
                    }
                    curr.push_back(result);
                    if (!skip) { 
                        if (solve(curr)) return true;
                    }
                    curr.pop_back();
                }
            }
        }
        return false;
    }
    
    // bool solve(vector<float> nums) {
    //     if (nums.size() == 0) return false;
    //     if (nums.size() == 1) return abs(nums[0] - 24) < 1e-6;

    //     for (int i = 0; i < nums.size(); i++) {
    //         for (int j = 0; j < nums.size(); j++) {
    //             if (i != j) {
    //                 vector<float> nums2;
    //                 for (int k = 0; k < nums.size(); k++) if (k != i && k != j) {
    //                     if (k == i || k == j) continue;
    //                     nums2.push_back(nums[k]);
    //                 }
    //                 for (int k = 0; k < 4; k++) {
    //                     if (k < 2 && j > i) continue;
    //                     if (k == 0) nums2.push_back(nums[i] + nums[j]);
    //                     if (k == 1) nums2.push_back(nums[i] * nums[j]);
    //                     if (k == 2) nums2.push_back(nums[i] - nums[j]);
    //                     if (k == 3) {
    //                         if (nums[j] != 0) {
    //                             nums2.push_back(nums[i] / nums[j]);
    //                         } else {
    //                             continue;
    //                         }
    //                     }
    //                     if (solve(nums2)) return true;
    //                     nums2.pop_back();
    //                 }
    //             }
    //         }
    //     }
    //     return false;
    // }

    bool judgePoint24(vector<int>& nums) {
        vector<float> float_nums;
        float_nums.reserve(nums.size());
        for (auto i : nums) {
            float_nums.push_back(i);
        }
        
        return solve(float_nums);
    }
};

void trimLeftTrailingSpaces(string &input) {
    input.erase(input.begin(), find_if(input.begin(), input.end(), [](int ch) {
        return !isspace(ch);
    }));
}

void trimRightTrailingSpaces(string &input) {
    input.erase(find_if(input.rbegin(), input.rend(), [](int ch) {
        return !isspace(ch);
    }).base(), input.end());
}

vector<int> stringToIntegerVector(string input) {
    vector<int> output;
    trimLeftTrailingSpaces(input);
    trimRightTrailingSpaces(input);
    input = input.substr(1, input.length() - 2);
    stringstream ss;
    ss.str(input);
    string item;
    char delim = ',';
    while (getline(ss, item, delim)) {
        output.push_back(stoi(item));
    }
    return output;
}

string boolToString(bool input) {
    return input ? "True" : "False";
}

int main() {
    string line;
    
    vector<int> nums = {4, 1, 8, 7};
    bool ret = Solution().judgePoint24(nums);

    string out = boolToString(ret);
    cout << out << endl;
    
    return 0;
}