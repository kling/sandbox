#include <iostream>
#include <math.h>

#include <vector>
#include <random>

void print_vector(std::vector<int>& vec) {
  int size = vec.size();
  std::cout << "Vector: " << size << " entries." << std::endl;

  int n = 0;
  for (int i = vec.size() - 1; i >= 0; i--) {
    std::cout << vec[n] << " ";
    n++;
  }
  std::cout << std::endl;

//  for (std::vector<int>::iterator it = vect.begin(); vect.end() != it; ++it) { }

}

// Runs an in place insertion sort
void run_insertion_sort(std::vector<int>& vect) {
  for (int i = 1; i < vect.size(); i++) {
    if (vect[i] < vect[i-1]) { // Check if it's actually out of place
      for (int j = 0; j < i; j++) {
        if (vect[j] > vect[i]) {
          int temp = vect[i];
          vect.erase(vect.begin() + i);
          vect.insert(vect.begin() + j, temp);
          break;
        }
      }
    }
  }
}

// Runs an in place bubble sort
void run_bubble_sort(std::vector<int>& vect) {
  for (int i = vect.size(); i > 0; --i) {
    bool no_swap = true;

    for (int j = 1; j < i; ++j) {
      if (vect[j-1] > vect[j]) {
        // swap
        int temp = vect[j];
        vect[j] = vect[j-1];
        vect[j-1] = temp;
        no_swap = false;
      }
    }

    if (no_swap) {
      break;
    }
  }
}


void run_merge_sort(std::vector<int>& vect) {
  if (vect.size() < 2) {
    return;
  }

  // Split into two
  int half_size = vect.size() / 2;
  std::vector<int> lower_half(vect.begin(), vect.begin()+half_size);
  std::vector<int> upper_half(vect.begin()+half_size, vect.end());

  // Sort two halves
  run_merge_sort(lower_half);
  run_merge_sort(upper_half);

  // Merge two halves
  std::vector<int>::iterator lower_iterator = lower_half.begin();
  std::vector<int>::iterator upper_iterator = upper_half.begin();

  for (int i = 0; i < vect.size(); i++) {
    int lower = *lower_iterator;
    int upper = *upper_iterator;

    if (upper_half.end() == upper_iterator ||
        (*lower_iterator <= *upper_iterator && lower_half.end() != lower_iterator)) {
      vect[i] = *lower_iterator;
      ++lower_iterator;
    } else {
      vect[i] = *upper_iterator;
      ++upper_iterator;
    }
  }
}


void run_quick_sort(std::vector<int>& vect) {
  if (vect.size() < 2) {
    return;
  }

  std::default_random_engine generator;
  std::uniform_int_distribution<int> distribution(0,vect.size());
  int pivot = vect[distribution(generator)];
//  int pivot = vect[1];


//  std::cout << "Pivot: " << pivot << std::endl;

  std::vector<int>::iterator front_iterator = vect.begin();
  std::vector<int>::iterator back_iterator = vect.end()-1;

//  std::cout << "Before pivot" << std::endl;
//  print_vector(vect);

  // Careful here! vect can contain duplicate values, so front_iterator and
  // back_iterator are only guaranteed to point to 'a' pivot, not a unique pivot

  while (front_iterator != back_iterator) {
    // Nudge front if both are stuck on pivot
    if (*front_iterator == pivot && *back_iterator == pivot) {
      ++front_iterator;
    }

    while (*front_iterator < pivot && front_iterator != back_iterator) {
//      std::cout << "Front skipping: " << *front_iterator << std::endl;
      ++front_iterator;
    }
    while (*back_iterator > pivot && front_iterator != back_iterator) {
//      std::cout << "Back skipping: " << *back_iterator << std::endl;
      --back_iterator;
    }

//    std::cout << "Swapping: " << *front_iterator << " and " << *back_iterator << std::endl;
    int temp = *front_iterator;
    *front_iterator = *back_iterator;
    *back_iterator = temp;

//    print_vector(vect);
  }

//  std::cout << std::endl << "After partition" << std::endl;
//  print_vector(vect);

  // Partitioning is done. front_iterator and back_iterator should now point to a pivot. Pivot number may
  // appear in both the top and bottom.
  // Sort the top and bottom halves if needed, the sorted versions into the vector to return.

  std::vector<int> front_partition;
  std::vector<int> back_partition;

  bool process_front = vect.begin() != front_iterator;
  bool process_back = vect.end()-1 != back_iterator;

  if (process_front) {
    front_partition = std::vector<int>(vect.begin(), front_iterator);
    run_quick_sort(front_partition);
  }
  if (process_back) {
    back_partition = std::vector<int>(back_iterator+1, vect.end());
    run_quick_sort(back_partition);
  }

  // This could be more efficient if i could sort subsets of the vector in place.
  // Would have to do a bit of refactoring for that.
  if (process_front) {
    vect.erase(vect.begin(), front_iterator);
    vect.insert(vect.begin(), front_partition.begin(), front_partition.end());
  }
  if (process_back) {
    vect.erase(back_iterator+1, vect.end());
    vect.insert(back_iterator+1, back_partition.begin(), back_partition.end());
  }
}


void run_heap_sort(std::vector<int>& vect) {
  // First create the heap. Consider a complete binary-tree with nodes ordered in breadth first order, root at 0.
  // Children of node n are at 2(n+1) and 2(n+1) - 1
  // The parent of node n is at (n-1)/2

  // Start at front and add things to the heap one by one
  for (int i = 1; i < vect.size(); ++i) {
    //percolate up
    int curr_index = i;
    int parent_index = (curr_index-1)/2;
    while (curr_index != 0 && vect[curr_index] > vect[parent_index]) {
      int temp = vect[parent_index];
      vect[parent_index] = vect[curr_index];
      vect[curr_index] = temp;

      curr_index = parent_index;
      parent_index = (curr_index-1)/2;
    }

//    std::cout << "Intermediate heap for i = " << i << std::endl;
//    print_vector(vect);
  }

//  std::cout << "STARTING ROOT POPPING" << std::endl;
//  print_vector(vect);

  // Now read out the heap into a sorted order
  for (int i = 0; i < vect.size(); ++i) {
    int next_back_to_front_insert_index = vect.size() - (i + 1);
    int temp = vect[next_back_to_front_insert_index];
    vect[next_back_to_front_insert_index] = vect[0];
    vect[0] = temp;

//    std::cout << "\n\nPopped front and inserted at: " << next_back_to_front_insert_index << std::endl;
//    print_vector(vect);

    // Percolate new root down
    int curr_parent_index = 0;
    int curr_left_child_index = 2*(curr_parent_index+1) - 1;
    int curr_right_child_index = 2*(curr_parent_index+1);

    while ((curr_left_child_index < next_back_to_front_insert_index && vect[curr_parent_index] < vect[curr_left_child_index]) ||
           (curr_right_child_index < next_back_to_front_insert_index && vect[curr_parent_index] < vect[curr_right_child_index])) {

//      std::cout << "Percolated down: " << curr_parent_index << std::endl;

      if (curr_right_child_index < next_back_to_front_insert_index && vect[curr_left_child_index] < vect[curr_right_child_index]) {
        // Move up the right child
        int temp2 = vect[curr_right_child_index];
        vect[curr_right_child_index] = vect[curr_parent_index];
        vect[curr_parent_index] = temp2;
        curr_parent_index = curr_right_child_index;
      } else {
        int temp3 = vect[curr_left_child_index];
        vect[curr_left_child_index] = vect[curr_parent_index];
        vect[curr_parent_index] = temp3;
        curr_parent_index = curr_left_child_index;
      }

      curr_left_child_index = 2*(curr_parent_index+1) - 1;
      curr_right_child_index = 2*(curr_parent_index+1);

//      print_vector(vect);
    }
  }
}

bool is_sorted(std::vector<int>& vect) {
  for (int i = vect.size()-1; i > 0; --i) {
    if (vect[i-1] > vect[i]) {
      return false;
    }
  }
  return true;
}

void print_is_sorted(std::vector<int>& vect) {
  if (is_sorted(vect)) {
    std::cout << "Vector is sorted." << std::endl;
  } else {
    std::cout << "Vector is NOT sorted." << std::endl;
  }
}

int main(int argc, char* argv[])
{
  std::cout << "==Start of Sorting Testbed==" << std::endl;

  std::default_random_engine generator;
  std::uniform_int_distribution<int> distribution(1,100);

  std::cout << "Generating random list..." << std::endl;
  std::vector<int> unsorted;
  for (int i = 0; i < 100000; i++) {
    unsorted.push_back(distribution(generator));
  }

  std::cout << "Unsorted list" << std::endl;
//  print_vector(unsorted);
  print_is_sorted(unsorted);

  {
    std::cout << "\nRun insertion sort..." << std::endl;
    std::vector<int> insertion_sorted = unsorted;
    run_insertion_sort(insertion_sorted);
  //  std::cout << "Sorted list" << std::endl;
  //  print_vector(insertion_sorted);
    print_is_sorted(insertion_sorted);
  }

  {
    std::cout << "\nRun bubble sort..." << std::endl;
    std::vector<int> bubble_sorted = unsorted;
    run_bubble_sort(bubble_sorted);
  //  std::cout << "Sorted list" << std::endl;
  //  print_vector(bubble_sorted);
    print_is_sorted(bubble_sorted);
  }

  {
    std::cout << "\nRun merge sort..." << std::endl;
    std::vector<int> merge_sorted = unsorted;
    run_merge_sort(merge_sorted);
  //  std::cout << "Sorted list" << std::endl;
  //  print_vector(merge_sorted);
    print_is_sorted(merge_sorted);
  }

  {
    std::cout << "\nRun quick sort..." << std::endl;
    std::vector<int> quick_sorted = unsorted;
    run_quick_sort(quick_sorted);
  //  std::cout << "Sorted list" << std::endl;
  //  print_vector(quick_sorted);
    print_is_sorted(quick_sorted);
  }

  {
    std::cout << "\nRun heap sort..." << std::endl;
    std::vector<int> heap_sorted = unsorted;
    run_heap_sort(heap_sorted);
//    std::cout << "Sorted list" << std::endl;
//    print_vector(heap_sorted);
    print_is_sorted(heap_sorted);
  }

  return 0;
}
