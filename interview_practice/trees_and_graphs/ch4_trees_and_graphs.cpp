#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <list>


struct Node;

typedef struct Node {
  std::vector<Node*> neighbours;
  int value;
} Node;


std::map<Node*, bool> visited_map;
std::list<Node*> to_explore;



// Adds all neighbours of a node to the list to explore.
void explore_node(Node* curr) {
  visited_map[curr] = true;

  for (auto n : curr->neighbours) {
    to_explore.push_back(n);
  }
}

bool are_conected(Node* a, Node* b) {
  to_explore.push_back(a);
  to_explore.push_back(b);

  while (!to_explore.empty()) {
    Node* curr = to_explore.front();
    to_explore.pop_front();

    if (visited_map.find(curr) != visited_map.end()) {
      return true;
    }
    explore_node(curr);
  }
  return false;
}

// struct TreeNode;
typedef struct TreeNode {
  int value;
  TreeNode* left;
  TreeNode* right;
} TreeNode;

// Q 4.2: Create minimal tree from sorted array.
TreeNode* minimal_tree(std::vector<int>& v, int start, int end) {
  if (start > end) return NULL;
  TreeNode* n = new TreeNode();
  int mid = (end + start) / 2;
  n->value = v[mid];
  n->left = minimal_tree(v, start, mid-1);
  n->right = minimal_tree(v, mid+1, end);
}

TreeNode* minimal_tree(std::vector<int>& v) {
   return minimal_tree(v, 0, v.size()-1);
}

std::vector<int> sorted_list;

void populate_sorted_list() {
  sorted_list.push_back(1);
  sorted_list.push_back(2);
  sorted_list.push_back(3);
  sorted_list.push_back(4);
  sorted_list.push_back(5);
  sorted_list.push_back(6);
  // sorted_list.push_back(7);
  // sorted_list.push_back(8);
  // sorted_list.push_back(9);
  // sorted_list.push_back(10);
  // sorted_list.push_back(11);
  // sorted_list.push_back(12);
  // sorted_list.push_back(13);
  // sorted_list.push_back(14);
  // sorted_list.push_back(15);
}


void tree_level_order_traverse(TreeNode* node, std::vector<std::list<int>* >& level_order, int level);
void tree_level_order_traverse(TreeNode* node, std::vector<std::list<int>* >& level_order, int level) {
  if (level >= level_order.size()) {
      level_order.push_back(new std::list<int>());
  }

  if (!node) {
    level_order[level]->push_back(-1);
    return;
  } else {
    level_order[level]->push_back(node->value);
  }

  tree_level_order_traverse(node->left, level_order, level+1);
  tree_level_order_traverse(node->right, level_order, level+1);
}

// Q4.3: List of depths
std::vector<std::list<int>* > tree_level_order(TreeNode* node) {
  std::vector<std::list<int>* > level_order;
  tree_level_order_traverse(node, level_order, 0);
  return level_order;
}

void print_level_order(std::vector<std::list<int>* >& level_order) {
  std::cout << "Printing level order list" << std::endl;
  for (auto l : level_order) {
    for (auto n : *l) {
      std::cout << n << ", ";
    }
    std::cout << std::endl;
  }
}

typedef struct {
  TreeNode* node;
  int level;
} NodeAndLevel;

bool check_balanced(TreeNode* root) {
  std::list<NodeAndLevel> to_explore;
  if (!root) return true;

  NodeAndLevel n;
  n.node = root;
  n.level = 0;
  to_explore.push_back(n);

  int shortest_level = -1;
  int current_level = 0;

  while (!to_explore.empty()) {
    NodeAndLevel curr = to_explore.front();
    to_explore.pop_front();


    if ((!curr.node->left || !curr.node->right) && shortest_level == -1) {
      shortest_level = curr.level;
    }

    current_level = curr.level + 1;

    if (curr.node->left) {
      NodeAndLevel left;
      left.node = curr.node->left;
      left.level = current_level;
      to_explore.push_back(left);
    }
    if (curr.node->right) {
      NodeAndLevel right;
      right.node = curr.node->right;
      right.level = current_level;
      to_explore.push_back(right);
    }

    if (shortest_level >= 0 && (current_level - shortest_level) >= 2) {
      return false;
    }
  }
  return true;
}


TreeNode* create_balanced_tree() {
  TreeNode* n4 = new TreeNode();
  n4->value = 4;
  n4->left = NULL;
  n4->right = NULL;

  TreeNode* n3 = new TreeNode();
  n3->value = 3;
  n3->left = n4;
  n3->right = n4;

  TreeNode* n2 = new TreeNode();
  n2->value = 2;
  n2->left = n4;
  n2->right = n4;

  TreeNode* n1 = new TreeNode();
  n1->value = 1;
  n1->left = n2;
  n1->right = n3;

  return n1;
}

TreeNode* create_unbalanced_tree() {
  TreeNode* n5 = new TreeNode();
  n5->value = 5;
  n5->left = NULL;
  n5->right = NULL;

  TreeNode* n4 = new TreeNode();
  n4->value = 4;
  n4->left = NULL;
  n4->right = n5;

  TreeNode* n3 = new TreeNode();
  n3->value = 3;
  n3->left = NULL;
  n3->right = NULL;

  TreeNode* n2 = new TreeNode();
  n2->value = 2;
  n2->left = NULL;
  n2->right = n4;

  TreeNode* n1 = new TreeNode();
  n1->value = 1;
  n1->left = n2;
  n1->right = n3;

  return n1;
}

// Q 4.5 Validate BST
bool validate_binary_search_tree(TreeNode* node) {
  if (!node) {
    return true;
  }
  
  bool check_left = true;
  if (node->left) {
    check_left = node->value >= node->left->value;
  }

  bool check_right = true;
  if (node->right) {
    check_right = node->value < node->right->value;
  }
  return check_left && check_right &&
      validate_binary_search_tree(node->left) && validate_binary_search_tree(node->right);
}


void print_sequence(std::list<int>& sequence) {
  for (auto& i : sequence) {
    std::cout << i << ", ";
  }
  std::cout << std::endl;
}


// index identifies the next node in open to explore.
void bst_sequences_explore_open_node(std::vector<TreeNode*> open, std::list<int> sequence, int index) {
  auto it = open.begin() + index;
  TreeNode* curr = *it;
  sequence.push_back(curr->value);
  if (curr->left) open.push_back(curr->left);
  if (curr->right) open.push_back(curr->right);
  open.erase(open.begin() + index);

  if (open.empty()) {
    print_sequence(sequence);
    return;
  }

  for (int i = 0; i < open.size(); i++) {
    bst_sequences_explore_open_node(open, sequence, i);
  }
}

void bst_sequences(TreeNode* root) {
  std::vector<TreeNode*> open_nodes;
  std::list<int> sequence;

  if (!root) return;
  open_nodes.push_back(root);
  bst_sequences_explore_open_node(open_nodes, sequence, 0);
}


int main() {
  std::cout << __cplusplus << std::endl;

  populate_sorted_list();
  auto min_tree = minimal_tree(sorted_list);

  auto level_order = tree_level_order(min_tree);
  print_level_order(level_order);

  TreeNode* n1 = create_balanced_tree();
  auto level_order_balanced = tree_level_order(n1);
  print_level_order(level_order_balanced);

  std::cout << "Validate BST Good: " << validate_binary_search_tree(min_tree) << std::endl;
  std::cout << "Validate BST Bad: " << validate_binary_search_tree(n1) << std::endl;

  std::cout << "Is tree balanced (yes): " << check_balanced(n1) << std::endl;
  std::cout << "Is tree balanced (no): " << check_balanced(create_unbalanced_tree()) << std::endl;

  std::cout << "\n\nBST Sequences\n";
  bst_sequences(min_tree);

  return 0;
}

